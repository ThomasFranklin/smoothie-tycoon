package com.tadgames.smoothietycoon;


import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tadgames.smoothietycoon.GameClasses.Recipe;
import com.tadgames.smoothietycoon.databinding.PlayerProfileScreenBinding;
import com.tadgames.smoothietycoon.databinding.RecipesBinding;

public class GameActivity extends Activity {

    private RelativeLayout mainLayout;
    private MainGameView gameView;
    private ImageHandler imageHandler;

    private DisplayMetrics dm;

    private View recipesView;

    private RecipeListAdapter recipeListAdapter;
    private View smoothieCreationView;
    private SmoothieCreationHandler smoothieCreationHandler;

    private View profileScreen;
    private ProfileScreenHandler profileHandler;

    public View overlay;

    private View suppliesScreen;
    private View objectPlacementOverlay;
    private SupplyScreenHandler supplyScreenHandler;
    private RecipeOptionsHandler recipeOptionsHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_game_view);
        mainLayout = (RelativeLayout) findViewById(R.id.main_game);

        dm = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);

        imageHandler = new ImageHandler(getApplicationContext(), dm);
        gameView = new MainGameView(getApplicationContext(), this, imageHandler, dm);
        mainLayout.addView(gameView);

        LayoutInflater overlayInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        overlay = overlayInflater.inflate(R.layout.game_overlay, null, false);
        mainLayout.addView(overlay);
        gameView.updateOverlay();
        overlay.setVisibility(View.VISIBLE);

        LayoutInflater recipesInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        recipesView = recipesInflater.inflate(R.layout.recipes, null, false);
        recipeListAdapter = new RecipeListAdapter(getApplicationContext(), gameView.getRecipes());
        ListView recipesListView = (ListView) recipesView.findViewById(R.id.recipesList);
        recipesListView.setAdapter(recipeListAdapter);
        recipesListView.setOnItemClickListener(recipeListListener);
        RecipesBinding recipesBinding = RecipesBinding.bind(recipesView);
        recipesBinding.setGameView(gameView);
        mainLayout.addView(recipesView);

        Button openRecipesScreen = (Button) findViewById(R.id.openRecipes);
        openRecipesScreen.setOnClickListener(this.openRecipesScreen);
        Button exitRecipesScreen = (Button) findViewById(R.id.exitRecipesScreen);
        exitRecipesScreen.setOnClickListener(this.exitRecipesScreen);

        LayoutInflater smoothieCreationInflater = (LayoutInflater)
                getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        smoothieCreationView = smoothieCreationInflater.inflate(R.layout.smoothie_creation, null, false);
        mainLayout.addView(smoothieCreationView);
        ListView createIngredientsList = (ListView) findViewById(R.id.ingredientListView);
        createIngredientsList.setAdapter(new SmoothieCreationIngredientListAdapter(
                gameView.getContext(), gameView.getIngredients()));
        Button backOutSmoothieButton = (Button) findViewById(R.id.backOutSmoothieCreate);
        backOutSmoothieButton.setOnClickListener(backOutCreateSmoothie);
        Button createNewSmoothie = (Button) findViewById(R.id.createNewSmoothie);
        createNewSmoothie.setOnClickListener(clickCreateSmoothie);

        smoothieCreationView.setVisibility(View.GONE);

        LayoutInflater supplyScreenInflator = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        suppliesScreen = supplyScreenInflator.inflate(R.layout.purchase_supplies_screen, null, false);
        mainLayout.addView(suppliesScreen);
        suppliesScreen.setVisibility(View.GONE);

        Button purchaseSuppliesButton = (Button) findViewById(R.id.purchaseSuppliesButton);
        purchaseSuppliesButton.setOnClickListener(clickPurchaseSupplies);

        LayoutInflater objectPlacementInflator = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        objectPlacementOverlay = objectPlacementInflator.inflate(R.layout.object_placement_overlay, null, false);
        mainLayout.addView(objectPlacementOverlay);
        objectPlacementOverlay.setVisibility(View.GONE);

        supplyScreenHandler = new SupplyScreenHandler(this, gameView, getApplicationContext(), suppliesScreen, objectPlacementOverlay, imageHandler);
        gameView.setSupplyScreenHandler(supplyScreenHandler);

        recipeOptionsHandler = new RecipeOptionsHandler(gameView, mainLayout, recipeListAdapter,
                getApplicationContext());

        inflatePlayerProfileScreen();
        Button profileButton = (Button) findViewById(R.id.viewProfile);
        profileButton.setOnClickListener(clickViewProfile);
    }

    OnClickListener openRecipesScreen = new OnClickListener() {
        @Override
        public void onClick(View v) {
            overlay.setVisibility(View.GONE);
            gameView.openRecipeScreen();
        }
    };

    OnClickListener exitRecipesScreen = new OnClickListener() {
        @Override
        public void onClick(View v) {
            gameView.closeRecipeScreen();
            overlay.setVisibility(View.VISIBLE);
        }
    };

    OnClickListener clickCreateSmoothie = new OnClickListener() {
        @Override
        public void onClick(View v) {
            gameView.closeRecipeScreen();
            smoothieCreationView.setVisibility(View.VISIBLE);
            smoothieCreationHandler = new SmoothieCreationHandler(getApplicationContext(),
                    smoothieCreationView, gameView, mainLayout, 15/*THIS NUMBER HERE IS A FILLER*/,
                    recipeListAdapter);
        }
    };

    OnClickListener backOutCreateSmoothie = new OnClickListener() {
        @Override
        public void onClick(View v) {
            TextView cost = (TextView) smoothieCreationView.findViewById(R.id.smoothieCost);
            cost.setText("Cost: 0");
            smoothieCreationView.setVisibility(View.GONE);
            smoothieCreationHandler = null;
            gameView.openRecipeScreen();
        }
    };

    OnClickListener clickPurchaseSupplies = new OnClickListener() {
        @Override
        public void onClick(View v) {
            overlay.setVisibility(View.GONE);
            supplyScreenHandler.initializeScreen();
            suppliesScreen.setVisibility(View.VISIBLE);
        }
    };

    OnClickListener clickViewProfile = new OnClickListener() {
        @Override
        public void onClick(View v) {
            overlay.setVisibility(View.GONE);
            profileHandler.updateProfileScreen();
            profileScreen.setVisibility(View.VISIBLE);
        }
    };

    public void exitProfileScreen(){
        profileScreen.setVisibility(View.GONE);
        overlay.setVisibility(View.VISIBLE);
    }

    private void inflatePlayerProfileScreen(){
        LayoutInflater profileInflater = getLayoutInflater();
        profileScreen = profileInflater.inflate(R.layout.player_profile_screen, null, false);
        PlayerProfileScreenBinding profileScreenBinding = PlayerProfileScreenBinding.bind(profileScreen);
        profileScreenBinding.setPlayer(gameView.getPlayer());
        mainLayout.addView(profileScreen);
        profileScreen.setVisibility(View.GONE);
        profileHandler = new ProfileScreenHandler(gameView.getPlayer(), this);
    }

    public ProfileScreenHandler getProfileHandler(){
        return profileHandler;
    }

    AdapterView.OnItemClickListener recipeListListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            // Open an overlay that allows price change or removing of recipe
            Recipe recipe = (Recipe) parent.getItemAtPosition(position);
            recipeOptionsHandler.openRecipeOptions(recipe);
        }
    };

    @Override
    protected void onPause() {
        gameView.storeGrid.saveStore(getApplicationContext());
        gameView.gameThread.setRunning(false);
        GameActivity.this.finish();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (SaveGameHandler.checkIfSaveExists(getApplicationContext())){
            gameView.storeGrid.loadStore(getApplicationContext());
        }
        gameView.gameThread.setRunning(true);
    }
}

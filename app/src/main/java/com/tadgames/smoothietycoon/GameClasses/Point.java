package com.tadgames.smoothietycoon.GameClasses;


public class Point {

    public int x;
    public int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point(float x, float y){
        this.x = (int) x;
        this.y = (int) y;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Point)){
            return false;
        }
        if (this.x == ((Point) o).x && this.y == ((Point) o).y)
            return true;
        return false;
    }
}

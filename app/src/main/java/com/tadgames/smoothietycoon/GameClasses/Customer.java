package com.tadgames.smoothietycoon.GameClasses;

public class Customer {
    public static final int LEFT = 1;
    public static final int RIGHT = 2;
    public static final int UP = 3;
    public static final int DOWN = 4;

    private Flavor tastes;
    private int healthinessDesired;
    private int maxAcceptibleCalories;
    private String name; // Name here just describes the type of smoothie drinker the customer is.
        // So for a health minded one the name might be healthyCustomer.
        // Hopefully something better than that.
        // ALSO USED AS A KEY FOR DRAWING IMAGE
    private float maxBudget;

    private int ticksInLine;
    private int ticksToFinishDrinking; // When it hits 0 the customer is done and will leave
    private Point position; // Position is not a point on canvas but a point on the store's grid.
    public Point nextPt;
    public Point destination; // Public because modification/access by CustomerHandler is necessary
    public float movementTick; // Err... yeah... this is a number to help out with "smoothly"
        // transitioning between points. Should go from 0 to 25;
    public int direction; // positive or negative 1. To help with math. Stored so don't have to
        // recalculate too much

    public Customer(int sweet, int tart, int spice, int bit, int sour, int nut, int salt,
                    int healthinessDesired, int maxAcceptibleCalories, String name,
                    float maxBudget, Point position){
        this.tastes = new Flavor(sweet, tart, spice, bit, sour, nut, salt);
        this.healthinessDesired = healthinessDesired;
        this.maxAcceptibleCalories = maxAcceptibleCalories;
        this.name = name;

        this.maxBudget = maxBudget;
        this.position = position;

        ticksInLine = 0;
        destination = null;
        movementTick = 0;
    }

    public Flavor getTastes(){
        return tastes;
    }

    public int getHealthinessDesired(){
        return healthinessDesired;
    }

    public int getMaxAcceptibleCalories(){
        return maxAcceptibleCalories;
    }

    public String getName(){
        return name;
    }

    public Point getPosition(){
        return position;
    }

    public void setPosition(Point pt){
        position = pt;
    }

    public float getMaxBudget(){
        return maxBudget;
    }

    // Should be set to a negative number, that way we don't have to keep track of another field
    public void setTicksToFinishDrinking(int i){
        ticksToFinishDrinking = -Math.abs(i);
    }

    public void upTicksInLine(){
        ticksInLine++;
    }

    public void upTicksDrinking(){
        ticksToFinishDrinking++;
    }

    public int getTicksInLine(){
        return ticksInLine;
    }

    public int getTicksToFinishDrinking(){
        return ticksToFinishDrinking;
    }
}

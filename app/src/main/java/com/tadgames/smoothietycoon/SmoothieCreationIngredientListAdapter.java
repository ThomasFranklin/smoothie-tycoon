package com.tadgames.smoothietycoon;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tadgames.smoothietycoon.GameClasses.Ingredient;

import java.util.List;

public class SmoothieCreationIngredientListAdapter extends ArrayAdapter<Ingredient>{
        private final Context context;

        public SmoothieCreationIngredientListAdapter(Context context, List<Ingredient> ingredients)
        {
            super(context, -1, ingredients);
            this.context = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = layoutInflater.inflate(R.layout.smthee_create_ingre_list, parent, false);
            TextView textView = (TextView) rowView.findViewById(R.id.ingredientName);
            ImageView imageView = (ImageView) rowView.findViewById(R.id.ingredientPic);

            textView.setText(getItem(position).getName());
            imageView.setImageResource(R.mipmap.strawberry);
            return rowView;
        }
}

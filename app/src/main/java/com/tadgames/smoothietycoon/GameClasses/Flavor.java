package com.tadgames.smoothietycoon.GameClasses;

import java.io.Serializable;

public class Flavor implements Serializable{
    private int sweetness; // Put what you want.
    private int tartness;
    private int spiciness;
    private int bitterness;
    private int sourness;
    private int nuttiness;
    private int saltiness;

    public Flavor(int sweetness, int tartness, int spiciness, int bitterness, int sourness,
                  int nuttiness, int saltiness){
        this.sweetness = sweetness;
        this.tartness = tartness;
        this.spiciness = spiciness;
        this.bitterness = bitterness;
        this.sourness = sourness;
        this.nuttiness = nuttiness;
        this.saltiness = saltiness;
    }

    // COPY CONSTRUCTOR
    public Flavor(Flavor flavorToCopy){
        this.sweetness = flavorToCopy.sweetness;
        this.tartness = flavorToCopy.tartness;
        this.spiciness = flavorToCopy.spiciness;
        this.bitterness = flavorToCopy.bitterness;
        this.sourness = flavorToCopy.sourness;
        this.nuttiness = flavorToCopy.nuttiness;
        this.saltiness = flavorToCopy.saltiness;
    }

    public static Flavor addFlavors(Flavor flavor1, Flavor flavor2){
        int sweet = flavor1.sweetness + flavor2.sweetness;
        int tart = flavor1.tartness + flavor2.tartness;
        int spice = flavor1.spiciness + flavor2.spiciness;
        int bit = flavor1.bitterness + flavor2.bitterness;
        int sour = flavor1.sourness + flavor2.sourness;
        int nut = flavor1.nuttiness + flavor2.nuttiness;
        int salt = flavor1.saltiness + flavor2.saltiness;

        return new Flavor(sweet, tart, spice, bit, sour, nut, salt);
    }

    public static Flavor subtractFlavors(Flavor flavor1, Flavor flavor2){
        int sweet = flavor1.sweetness - flavor2.sweetness;
        int tart = flavor1.tartness - flavor2.tartness;
        int spice = flavor1.spiciness - flavor2.spiciness;
        int bit = flavor1.bitterness - flavor2.bitterness;
        int sour = flavor1.sourness - flavor2.sourness;
        int nut = flavor1.nuttiness - flavor2.nuttiness;
        int salt = flavor1.saltiness - flavor2.saltiness;

        return new Flavor(sweet, tart, spice, bit, sour, nut, salt);
    }

    public static int compareFlavors(Flavor flavor1, Flavor flavor2){
        int sweet = flavor1.sweetness - flavor2.sweetness;
        int tart = flavor1.tartness - flavor2.tartness;
        int spice = flavor1.spiciness - flavor2.spiciness;
        int bit = flavor1.bitterness - flavor2.bitterness;
        int sour = flavor1.sourness - flavor2.sourness;
        int nut = flavor1.nuttiness - flavor2.nuttiness;
        int salt = flavor1.saltiness - flavor2.saltiness;

        int flavorDeviation = Math.abs(sweet) + Math.abs(tart) + Math.abs(spice) + Math.abs(bit) +
                Math.abs(sour) + Math.abs(nut) + Math.abs(salt);
        // when comparing, zero is the desired value for flavor deviation!
        return flavorDeviation;
    }
}

package com.tadgames.smoothietycoon.GameClasses;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class Achievement {
    private String name;
    private String description;
    private boolean achievedHuh;

    public String playerField; // The field from player class we are checking
    public String subField; // Potential subfield we might deal with
    public String numberRequired;


    public Achievement(String name, String description){
        this.name = name;
        this.description = description;
        achievedHuh = false;
    }

    public Achievement(String name, String description, boolean achievedHuh){
        this.name = name;
        this.description = description;
        this.achievedHuh = achievedHuh;
    }

    public void achieved(){
        this.achievedHuh = true;
    }

    public static ArrayList<Achievement> loadAchievements(Scanner achievementLib){
        ArrayList<Achievement> achievements = new ArrayList<>();

        while(achievementLib.hasNextLine()){
            String[] line = achievementLib.nextLine().split("%");
            String[] firstString = line[0].split(" ");
            if (firstString[0].equals("#"))
                continue;
            if (line.length < 4)
                continue;

            Achievement a = new Achievement(line[0], line[1]);
            a.numberRequired = line[2];
            a.playerField = line[3];

            if (line.length == 5)
                a.subField = line[4];

            achievements.add(a);
        }

        return achievements;
    }
}

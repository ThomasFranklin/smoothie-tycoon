package com.tadgames.smoothietycoon;

import android.content.Context;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileManager {

    private static final String BACKGRND = "default_background.txt";
    private static final String WALLS = "default_walls.txt";

    public static String[][] initializeBackground(Context context, int ROWS, int COLS){
        return loadFile(BACKGRND, context, ROWS, COLS);
    }

    public static String[][] initializeWalls(Context context, int ROWS, int COLS){
        return loadFile(WALLS, context, ROWS, COLS);
    }

    private static String[][] loadFile(String fileName, Context context, int ROWS, int COLS){
        String[][] gridData = new String[ROWS][];
        InputStream inputStream;
        try {
            inputStream = context.getAssets().open(fileName);
        }
        catch (FileNotFoundException e){
            System.out.println("File could not be found");
            return gridData;
        }
        catch (IOException e) {
            System.out.println("File could not be closed");
            return gridData;
        }
        Scanner input = new Scanner(inputStream);
        for(int ithRow = 0 ; ithRow < ROWS ; ithRow++) {
            gridData[ithRow] = new String[COLS];
            for (int jthCol = 0; jthCol < COLS; jthCol++) {
                if (input.hasNextLine()){
                    String line = input.nextLine();
                    if (line.equals("null")){
                        gridData[ithRow][jthCol] = null;
                    }
                    else {
                        gridData[ithRow][jthCol] = line;
                    }
                }
                else {
                    System.out.println("File formatted incorrectly");
                }
            }
        }
        input.close();
        try {
            inputStream.close();
        }
        catch (IOException e){
            System.out.println("File could not be closed");
        }
        return gridData;
    }

    public static List<String[]> readGenericFile(String filename, Context context){
        List<String[]> result = new ArrayList();
        try{
            InputStream inputStream = context.getAssets().open(filename);
            Scanner scanner = new Scanner(inputStream);
            while (scanner.hasNextLine()){
                String[] line = scanner.nextLine().split("%");
                if (!(line[0].equals("#"))){
                    result.add(line);
                }
            }
            scanner.close();
            inputStream.close();
        }
        catch (IOException e){
            System.err.println("FILE FAILED TO OPEN");
            e.printStackTrace();
        }
        return result;
    }
}

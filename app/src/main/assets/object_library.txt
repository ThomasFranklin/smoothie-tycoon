#% Used to store data for all objects in the supply screen and in the store
#% Will be updated and reformatted later
#% Current format:
#% Name%ResourceID%Cost%UnlockLevel
#% String%String%Int%Int
Table%place_holder_generic%50%1
Chair%place_holder_generic%20%1
Trash Can%place_holder_generic%15%1
Blender%place_holder_generic%50%1
Cash Register%cash_register%65%1
Stool%place_holder_generic%15%1

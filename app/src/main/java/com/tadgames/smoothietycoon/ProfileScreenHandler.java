package com.tadgames.smoothietycoon;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.tadgames.smoothietycoon.GameClasses.Recipe;

public class ProfileScreenHandler {

    private PlayerProfile player;
    private GameActivity gameActivity;

    private ImageView profilePic;
    private TextView experienceToNextLevel;

    private Button expandAllAchievements;
    private TextView numAvailableAP; // AP = Achievement Points

    private ImageView bestSmoothiePic;
    private TextView bestSmoothieName;
    private TextView numBestSoldSmoothie;

    private Button customerReviews; // Will inflate a list of responses customers have for smoothies
    private TextView avgReview; // errrrr...... it's an image right now.....

    private ImageView exitProfileScreen;

    private Button resetGame; // el oh el..... might be useful in testing later? maybe?
        // eh who would use this....

    public ProfileScreenHandler(PlayerProfile player, GameActivity gameActivity){
        this.player = player;
        this.gameActivity = gameActivity;

        profilePic = (ImageView) gameActivity.findViewById(R.id.profilePicture);
        experienceToNextLevel = (TextView) gameActivity.findViewById(R.id.profileExpToNextLevel);

        expandAllAchievements = (Button) gameActivity.findViewById(R.id.achievementsButton);
        numAvailableAP = (TextView) gameActivity.findViewById(R.id.profileAP);

        bestSmoothiePic = (ImageView) gameActivity.findViewById(R.id.mostSuccessfulSmoothiePic);
        bestSmoothieName = (TextView) gameActivity.findViewById(R.id.mostSuccessfulName);
        numBestSoldSmoothie = (TextView) gameActivity.findViewById(R.id.mostSuccessfulNumSold);

        customerReviews = (Button) gameActivity.findViewById(R.id.reviewsButton);
        avgReview = (TextView) gameActivity.findViewById(R.id.averageRating);

        resetGame = (Button) gameActivity.findViewById(R.id.resetGame);

        exitProfileScreen = (ImageView) gameActivity.findViewById(R.id.exitProfileScreen);
        exitProfileScreen.setOnClickListener(exitClickListener);

        updateProfileScreen();
    }

    public void updateProfileScreen(){
        // SET PROFILE PIC HERE

        avgReview.setText(String.valueOf(player.getAvgRating()));
        experienceToNextLevel.setText(player.getExperienceTilNextLevel() + " exp until level up");

        /* Achievement stuff
            here....
         */

        updateBestSmoothie();
    }

    View.OnClickListener exitClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            gameActivity.exitProfileScreen();
        }
    };

    private void updateBestSmoothie(){
        Recipe bestSmoothie = player.getMostSoldSmoothie();
        if (bestSmoothie != null) {
            // SET BEST SMOOTHIE PIC
            bestSmoothieName.setText(bestSmoothie.getName()); // REPLACE WITH
            // NAME WHEN THAT IS IMPLEMENTED FOR RECIPES
            numBestSoldSmoothie.setText(Integer.toString(bestSmoothie.getNumberSold()));
        }
        else {
            bestSmoothieName.setText("No Smoothies Sold Yet!");
            numBestSoldSmoothie.setText("0");
        }
    }

    // To be used when selling recipes from CustomerHandler
    public void updateSoldRecipes(){
        gameActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateBestSmoothie();
            }
        });
    }
}

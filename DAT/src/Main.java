import processing.core.PApplet;
import java.util.ArrayList;
import java.util.List;

public class Main extends PApplet{

    private final int TILE_WIDTH = 50;
    private final int TILE_HEIGHT = 25;

    private final int WALL_WIDTH = 50;
    private final int WALL_HEIGHT = 75;

    //Number of rows and columns for the entire world grid
    private final int ROWS = 46;
    private final int COLS = 23;

    private String[][] backgroundTiles;
    private String[][] walls;
    private String[][] defaultItems;
    private int[][] tileXPosn;
    private int[][] tileYPosn;

    private ImageHandler imageHandler;

    private int selectorXPosn;
    private int selectorYPosn;

    private String menuState;
    private int menuSelectorIndex;
    private List<String> menuOptions;

    private List<String> backgroundOptions;
    private List<String> wallOptions;

    public static void main(String[] args){
        PApplet.main("Main");
    }

    public void setup(){
        backgroundTiles = FileManager.initializeBackground(ROWS, COLS);
        initializePositions();
        walls = FileManager.initializeWalls(ROWS, COLS);
        imageHandler = new ImageHandler();
        selectorXPosn = 0;
        selectorYPosn = 0;

        menuState = "closed";

        menuOptions = new ArrayList<>();
        menuOptions.add("Change Background");
        menuOptions.add("Insert Wall");
        menuOptions.add("Insert Object");
        menuOptions.add("Clear Tile");
        menuOptions.add("Close");
        menuSelectorIndex = 0;

        backgroundOptions = FileManager.loadBackgroundOptions();
        wallOptions = FileManager.loadWallOptions();
        imageHandler.loadImages(wallOptions);


        System.out.println("\n\n**Instructions**\n");
        System.out.println("Arrow keys - Move selection");
        System.out.println("Enter - Select tile/Select option");
        System.out.println("S - Save world file");
        System.out.println("R - Reload world file (reset unsaved changes)");
        System.out.println("D - Delete saved world file/Create new world\n");
    }

    private void initializePositions(){
        this.tileXPosn = new int[ROWS][];
        this.tileYPosn = new int[ROWS][];
        int xConstant = (TILE_WIDTH / 2);
        int yConstant = ((TILE_HEIGHT / 2));
        for (int i = 0 ; i < ROWS ; i++) {
            tileXPosn[i] = new int[COLS];
            tileYPosn[i] = new int[COLS];
            for (int j = 0; j < COLS; j++) {
                if (i % 2 != 0) {
                    tileXPosn[i][j] = TILE_WIDTH * j + xConstant;
                    tileYPosn[i][j] = yConstant * i;
                }
                else {
                    tileXPosn[i][j] = TILE_WIDTH * j;
                    tileYPosn[i][j] = yConstant * i;
                }
            }
        }
       System.out.println("Tile positions updated.");
    }

    public void draw(){
        background(200);
        for (int i = 0 ; i < ROWS ; i++){
            for (int j = 0 ; j < COLS ; j++){
                String img = backgroundTiles[i][j];
                String wall = walls[i][j];
                image(imageHandler.getImage(img), tileXPosn[i][j], tileYPosn[i][j]);
                if (wall != null){
                    image(imageHandler.getImage(wall), tileXPosn[i][j], tileYPosn[i][j] + TILE_HEIGHT - WALL_HEIGHT);
                }
            }
        }
        image(imageHandler.getImage("selector"), tileXPosn[selectorYPosn][selectorXPosn], tileYPosn[selectorYPosn][selectorXPosn]);
        if (menuState.equals("menu")){
            fill(255, 255, 255);
            rect(0, 0, 400, 800);
            for (int i = 0 ; i < menuOptions.size() ; i++){
                if (menuSelectorIndex == i){
                    fill(251, 176, 59);
                }
                else{
                    fill(0, 0, 0);
                }
                textSize(25);
                text(menuOptions.get(i), 100, 100 + 100 * i);
            }
        }
        else if (menuState.equals("background")){
            fill(255, 255, 255);
            rect(0, 0, 400, 800);
            for (int i = 0 ; i < backgroundOptions.size() ; i++){
                if (menuSelectorIndex == i){
                    fill(251, 176, 59);
                }
                else{
                    fill(0, 0, 0);
                }
                textSize(25);
                text(backgroundOptions.get(i), 100, 100 + 100 * i);
            }
        }
        else if (menuState.equals("walls")){
            fill(255, 255, 255);
            rect(0, 0, 400, 800);
            for (int i = 0 ; i < wallOptions.size() ; i++){
                if (menuSelectorIndex == i){
                    fill(251, 176, 59);
                }
                else{
                    fill(0, 0, 0);
                }
                textSize(25);
                text(wallOptions.get(i), 20, 50 + 50 * i);
            }
        }
    }

    public void keyPressed(){
        if (menuState.equals("closed")){
            switch (keyCode){
                case LEFT:
                    selectorXPosn = Math.max(0, selectorXPosn - 1);
                    break;
                case RIGHT:
                    selectorXPosn = Math.min(COLS - 1, selectorXPosn + 1);
                    break;
                case UP:
                    selectorYPosn = Math.max(0, selectorYPosn - 1);
                    break;
                case DOWN:
                    selectorYPosn = Math.min(ROWS - 1, selectorYPosn + 1);
                    break;
                case ENTER:
                    menuState = "menu";
                    break;
                case 'R':
                    setup();
                    break;
                case 'S':
                    FileManager.saveBackground(backgroundTiles, ROWS, COLS);
                    FileManager.saveWalls(walls, ROWS, COLS);
                    System.out.println("All files saved. Please copy them to the app's asset folder.");
                    break;
                case 'D':
                    FileManager.deleteFiles();
                    System.out.println("All saved files have been deleted.");
                    setup();
                    break;
            }
        }
        else if (menuState.equals("menu")){
            switch (keyCode){
                case UP:
                    menuSelectorIndex = Math.max(0, menuSelectorIndex - 1);
                    break;
                case DOWN:
                    menuSelectorIndex = Math.min(menuOptions.size() - 1, menuSelectorIndex + 1);
                    break;
                case ENTER:
                    if (menuSelectorIndex == 0){
                        menuState = "background";
                        menuSelectorIndex = 0;
                    }
                    else if (menuSelectorIndex == 1){
                        menuState = "walls";
                        menuSelectorIndex = 0;
                    }
                    else if (menuSelectorIndex == 4){
                        menuState = "closed";
                        menuSelectorIndex = 0;
                    }
                    break;
            }
        }
        else if (menuState.equals("background")){
            switch (keyCode){
                case UP:
                    menuSelectorIndex = Math.max(0, menuSelectorIndex - 1);
                    break;
                case DOWN:
                    menuSelectorIndex = Math.min(backgroundOptions.size() - 1, menuSelectorIndex + 1);
                    break;
                case ENTER:
                    backgroundTiles[selectorYPosn][selectorXPosn] = backgroundOptions.get(menuSelectorIndex);
                    menuState = "closed";
                    menuSelectorIndex = 0;
                    break;
            }
        }
        else if (menuState.equals("walls")){
            switch (keyCode){
                case UP:
                    menuSelectorIndex = Math.max(0, menuSelectorIndex - 1);
                    break;
                case DOWN:
                    menuSelectorIndex = Math.min(wallOptions.size() - 1, menuSelectorIndex + 1);
                    break;
                case ENTER:
                    walls[selectorYPosn][selectorXPosn] = wallOptions.get(menuSelectorIndex);
                    menuState = "closed";
                    menuSelectorIndex = 0;
                    break;
            }
        }
    }
}

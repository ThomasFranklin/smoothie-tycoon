package com.tadgames.smoothietycoon;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.DisplayMetrics;

import java.util.HashMap;
import java.util.List;

public class ImageHandler {

    private float scaledTileWidth;
    private float scaledTileHeight;
    private float scaledWallHeight;
    private float scaledObjectHeight;

    private HashMap<String, Bitmap> backgroundImgs;
    private HashMap<String, Bitmap> wallImgs;
    private HashMap<String, Bitmap> objectImgs;
    private HashMap<String, Integer> objectIDs;
    private HashMap<String, Bitmap> fruitImgs;
    private HashMap<String, Bitmap> customerImgs;

    //This constructor is just for testing, will be changed later.
    public ImageHandler(Context context, DisplayMetrics displayMetrics){
        int screenWidth = displayMetrics.widthPixels;
        int screenHeight = displayMetrics.heightPixels;

        final String PACKAGE_NAME = "com.tadgames.smoothietycoon";

        System.out.println("Width is " + screenWidth);
        System.out.println("Height is " + screenHeight);

        scaledTileHeight = (float) (screenHeight / 10.0);
        scaledTileWidth = (float) (2.0 * scaledTileHeight);

        scaledWallHeight = (float) (scaledTileWidth * (75.0 / 50.0));
        scaledObjectHeight = (float) (scaledTileWidth * (40.0 / 50.0));

        backgroundImgs = new HashMap<>();
        backgroundImgs.put("tile",scaleBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.tile_orange_yellow), scaledTileWidth));
        backgroundImgs.put("grass", scaleBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.grass), scaledTileWidth));
        backgroundImgs.put("selector", scaleBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.selector), scaledTileWidth));

        wallImgs = new HashMap<>();
        List<String[]> wallLibrary = FileManager.readGenericFile("wall_library.txt", context);
        for (String[] line: wallLibrary){
            int resID = context.getResources().getIdentifier(PACKAGE_NAME + ":mipmap/" + line[0], null, null);
            wallImgs.put(line[0], scaleBitmap(BitmapFactory.decodeResource(context.getResources(),
                    resID), scaledTileWidth));
        }

        objectImgs = new HashMap<>();
        objectIDs = new HashMap<>();
        objectIDs.put(null, R.mipmap.place_holder_generic);
        List<String[]> objectLibrary = FileManager.readGenericFile("object_library.txt", context);
        for (String[] line: objectLibrary){
            int resID = context.getResources().getIdentifier(PACKAGE_NAME + ":mipmap/" + line[1], null, null);
            objectImgs.put(line[0], scaleBitmap(BitmapFactory.decodeResource(context.getResources(),
                    resID), scaledTileWidth));
            objectIDs.put(line[0], resID);
        }

        customerImgs = new HashMap<>();
        customerImgs.put("healthy", scaleBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.neon_customer), scaledTileWidth));
        customerImgs.put("regular", scaleBitmap(BitmapFactory.decodeResource(context.getResources(),
                R.mipmap.neon_customer), scaledTileWidth));
    }

    private Bitmap scaleBitmap(Bitmap sourceBitmap, float newWidth){
        int width = sourceBitmap.getWidth();
        int height = sourceBitmap.getHeight();
        float scaleFactor = newWidth / width;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleFactor, scaleFactor);
        return Bitmap.createBitmap(sourceBitmap, 0, 0, width, height, matrix, false);
    }

    public Bitmap getBackgroundImage(String string){
        return backgroundImgs.get(string);
    }

    public int getTileWidth(){
        return (int) scaledTileWidth;
    }

    public int getTileHeight(){
        return (int) scaledTileHeight;
    }

    public Bitmap getWallImage(String string){
        return wallImgs.get(string);
    }

    public Bitmap getStoreObjectImage(String string){
        return objectImgs.get(string);
    }

    public int getStoreObjectID(String name){
        return objectIDs.get(name);
    }

    public Bitmap getCustomerImage(String string){
        return customerImgs.get(string);
    }

    public int getWallHeight() {
        return (int) scaledWallHeight;
    }

    public int getObjectHeight(){
        return (int) scaledObjectHeight;
    }
}

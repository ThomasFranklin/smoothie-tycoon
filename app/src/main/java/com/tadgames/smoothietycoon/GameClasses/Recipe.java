package com.tadgames.smoothietycoon.GameClasses;

import java.io.Serializable;
import java.util.LinkedList;

public class Recipe implements Serializable{
    private LinkedList<Ingredient> ingredients;
    private float price;
    private float cost;
    private int healthiness;
    private Flavor smoothieFlavor;

    private int numberSold;

    private String name;

    public Recipe(){
        cost = 0;
        ingredients = new LinkedList<>();
        price = 0;
        healthiness = 0;
        smoothieFlavor = new Flavor(0,0,0,0,0,0,0);

        numberSold = 0;
    }

    // COPY CONSTRUCTOR to avoid aliasing error in code
    public Recipe(Recipe recipeToCopy){
        this.cost = recipeToCopy.cost;
        this.ingredients = new LinkedList<>(recipeToCopy.ingredients);
        this.price = recipeToCopy.price;
        this.healthiness = recipeToCopy.healthiness;
        this.smoothieFlavor = new Flavor(recipeToCopy.smoothieFlavor);
        this.numberSold = recipeToCopy.numberSold;
    }

    public void addIngredient(Ingredient ingredient){
        ingredients.add(ingredient);
        cost += ingredient.getCost();
        healthiness += ingredient.getHealthiness();
        smoothieFlavor = Flavor.addFlavors(smoothieFlavor, ingredient.getFlavor());
    }

    public void removeIngredient(Ingredient ingredient){
        ingredients.remove(ingredient);
        cost -= ingredient.getCost();
        healthiness -= ingredient.getHealthiness();
        smoothieFlavor = Flavor.subtractFlavors(smoothieFlavor, ingredient.getFlavor());
    }

    public Float getCost(){
        return cost;
    }

    public void setPrice(float price){
        this.price = price;
    }

    public Float getPrice() {
        return price;
    }

    public int getHealthiness(){
        return healthiness;
    }

    public Flavor getSmoothieFlavor(){
        return smoothieFlavor;
    }

    public void smoothieWasSold(){
        numberSold++;
    }

    public int getNumberSold(){
        return numberSold;
    }

    public void generateName(){
        int i = 0;
        String[] strings = new String[2];
        for (Ingredient ingredient : ingredients){
            if (i < 2){
                strings[i] = ingredient.getName();
                i++;
            }
        }
        if (strings.length == 2)
            name = strings[0] + " and " + strings[1] + " Smoothie";
        else
            name = strings[0] + "Smoothie";
    }

    public String getName(){
        return name;
    }

    public void removeAllIngredients(){
        ingredients = new LinkedList<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (!(o instanceof Recipe))
            return false;

        Recipe that = (Recipe) o;

        if (that.price == this.price && that.ingredients.containsAll(this.ingredients) &&
                this.cost == that.cost)
            return true;

        return false;
    }
}
import processing.core.PApplet;
import processing.core.PImage;
import java.util.HashMap;
import java.util.List;

public class ImageHandler extends PApplet{
    private HashMap<String, PImage> imageStore;
    private final String PATH = "D:/Documents/AndroidStudioProjects/SmoothieTycoon/app/src/main/res/mipmap-xxhdpi/";

    public ImageHandler(){
        imageStore = new HashMap<>();
        imageStore.put("selector", loadImage("selector.png"));
        imageStore.put("tile", loadImage(PATH + "tile_orange_yellow.png"));
        imageStore.put("grass", loadImage( PATH + "grass.png"));
    }
    public void loadImages(List<String> imageNames){
        for (String name : imageNames){
            imageStore.put(name, loadImage(PATH + name + ".png"));
        }
    }

    public PImage getImage(String key){
        return imageStore.get(key);
    }
}

package com.tadgames.smoothietycoon;

import android.content.Context;

import com.tadgames.smoothietycoon.GameClasses.Achievement;
import com.tadgames.smoothietycoon.GameClasses.Recipe;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class PlayerProfile implements Serializable{
    private float money;

    private int level;
    private int experience;
    private int experienceToLevelUp;

    private String companyName;
    private LinkedList<Recipe> recipes;

    // STATS
    private int smoothiesSold;
    private float totalMoneyEarned;
    private int customersVisited;
    private int happyFaces;
    private float avgRating;

    private ArrayList<Achievement> achievements;

    // USE THIS ONLY IF THERE IS NO EXISTING PLAYER PROFILE TO LOAD!!!!!!!!!!!
    public PlayerProfile(Context context){
        money = 0;
        level = 1;
        experience = 0;
        experienceToLevelUp = calculateExpToLevelUp();

        companyName = "TaD Smoothies";
        recipes = new LinkedList<>();

        smoothiesSold = 0;
        totalMoneyEarned = 0;

        customersVisited = 0;
        happyFaces = 0;
        avgRating = 0;

        try{
            InputStream achieveLib = context.getAssets().open("achievements.txt");
            achievements = Achievement.loadAchievements(new Scanner(achieveLib));
            achieveLib.close();
        }catch (IOException e){
            System.out.println("Achievements List Not Found");
            achievements = null;
        }
    }

    public void soldSmoothie(float price){
        money += price;
        totalMoneyEarned += price;
        addExperience(price);

        smoothiesSold++;
    }

    public void getHappyFace(){
        happyFaces++;
    }

    public void getFrownyFace(){
        happyFaces--;
    }

    public float getAvgRating(){
        if (customersVisited == 0)
            return 0;

        avgRating = (float) happyFaces / (float) customersVisited;
        float trunkedRating = (float) Math.floor(avgRating * 100) / 100;
        return trunkedRating;
    }

    public void upCustomersVisited(){
        customersVisited++;
    }

    public void spendMoney(float cost){
        money -= cost;
    }

    public float getMoney(){
        return money;
    }

    public int getSmoothiesSold(){
        return smoothiesSold;
    }

    public float getTotalMoneyEarned(){
        return totalMoneyEarned;
    }

    public int getLevel(){
        return level;
    }

    public String getLevelAsString(){
        return Integer.toString(level);
    }

    public int getExperience(){
        return experience;
    }

    public int getExperienceToLevelUp(){
        return experienceToLevelUp;
    }

    public int getExperienceTilNextLevel(){
        return experienceToLevelUp - experience;
    }

    public String getCompanyName(){
        return companyName;
    }

    private int calculateExpToLevelUp(){
        return 1000 + (level - 1) * 100;
    }

    private void addExperience(float price) {
        experience += price;
        while (experience >= experienceToLevelUp) {
            experience -= experienceToLevelUp;
            level++;
            experienceToLevelUp = calculateExpToLevelUp();
        }
    }

    public void addRecipe(Recipe recipe){
        recipes.add(recipe);
    }

    public void removeRecipe(Recipe recipe){
        recipes.remove(recipe);
    }

    public List<Recipe> getRecipes(){
        return recipes;
    }

    public Recipe getMostSoldSmoothie(){
        int mostSold = -1;
        Recipe mostSuccessful = null;

        for (Recipe recipe : recipes){
            int rNum = recipe.getNumberSold();
            if (rNum > mostSold){
                mostSold = rNum;
                mostSuccessful = recipe;
            }
        }

        return mostSuccessful; // THIS RETURN VALUE SHOULD NEVER BE NULL SO DON'T WORRY ABOUT IT
    }
}

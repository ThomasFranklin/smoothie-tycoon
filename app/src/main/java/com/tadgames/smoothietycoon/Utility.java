package com.tadgames.smoothietycoon;

import com.tadgames.smoothietycoon.GameClasses.Customer;
import com.tadgames.smoothietycoon.GameClasses.Ingredient;
import com.tadgames.smoothietycoon.GameClasses.Point;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Utility {

    public static LinkedList<Ingredient> loadIngredients(Scanner ingredientLib){
        LinkedList<Ingredient> ingredients = new LinkedList<>();
        int lineNum = 0;

        while (ingredientLib.hasNextLine()){
            String[] line = ingredientLib.nextLine().split(" ");
            lineNum++;
            if (line[0].equals("#"))
                continue;
            else if (line.length != 13){
                System.out.println("Error with ingredient on line" + lineNum);
                continue;
            }
            int size = Integer.parseInt(line[1]);
            int sweet = Integer.parseInt(line[2]);
            int tart = Integer.parseInt(line[3]);
            int spice = Integer.parseInt(line[4]);
            int bit = Integer.parseInt(line[5]);
            int sour = Integer.parseInt(line[6]);
            int nut = Integer.parseInt(line[7]);
            int salt = Integer.parseInt(line[8]);
            int cal = Integer.parseInt(line[9]);
            int health = Integer.parseInt(line[10]);
            float cost = Float.parseFloat(line[12]);
            ingredients.addLast(new Ingredient(line[0], size, sweet, tart, spice, bit, sour, nut,
                    salt, cal, health, line[11], cost));
        }
        System.out.println("Ingredient list size: " + ingredients.size());
        return ingredients;
    }

    public static Ingredient findIngredientFromListByName(String name, List<Ingredient> ingredients){

        for (Ingredient i: ingredients){
            if (i.getName().equals(name)){
                return i;
            }
        }

        throw new NoSuchElementException();
    }

    public static int clamp(int max, int min, int variable){
        return Math.min(Math.max(min, variable), max);
    }

    public static Point aStarToNextPos(Customer customer, StoreGrid storeGrid){
        LinkedList<AStarData> closedSet = new LinkedList<>();
        LinkedList<AStarData> openSet = new LinkedList<>();
        Point start = customer.getPosition();
        Point dest = customer.destination;
        AStarData aStart = new AStarData(start, 0);
        aStart.fScore = aStart.gScore + heuristicCost(aStart.point, dest);

        openSet.add(aStart);

        while (openSet.size() > 0){
            AStarData current = findLowestFScore(openSet);
            if (current.point.equals(dest)) {
                return nextPositionInPath(current);
            }
            closedSet.add(current);
            openSet.remove(current);

            List<AStarData> neighborNodes = findValidNeighborNodes(current, dest, storeGrid);
            for (AStarData neighbor: neighborNodes){
                if (closedSet.contains(neighbor)){
                    continue;
                }
                int tentativeGScore = current.gScore + 1;
                if (!openSet.contains(neighbor)){
                    neighbor.gScore = tentativeGScore;
                    neighbor.fScore = tentativeGScore + heuristicCost(neighbor.point, dest);
                    openSet.add(neighbor);
                }
            }
        }
        return null;
    }

    public static Point nextPositionInPath(AStarData current){
        AStarData nodeTraceBack = current;
        /*If this condition is true, then nodeTraceBack is the first step,
        nodeTraceBack.from is the starting node,
        and nodeTraceBack.from.from is null*/
        while((nodeTraceBack.from != null)&&(nodeTraceBack.from.from != null)){
            nodeTraceBack = nodeTraceBack.from;
        }
        return nodeTraceBack.point;

    }

    public static int heuristicCost(Point start, Point goal){
        int dx = Math.abs(goal.x - start.x);
        int dy = Math.abs(goal.y - start.y);
        return dx + dy;
    }

    private static List<AStarData> findValidNeighborNodes(AStarData current, Point dest,
                                                          StoreGrid storeGrid) {
        List<Point> neighborPts = new LinkedList<>();
        List<AStarData> neighborNodes = new LinkedList<>();
        neighborPts.add(new Point(current.point.x + 1, current.point.y));
        neighborPts.add(new Point(current.point.x - 1, current.point.y));
        neighborPts.add(new Point(current.point.x, current.point.y + 1));
        neighborPts.add(new Point(current.point.x, current.point.y - 1));

        for (Point pt: neighborPts){
            if (storeGrid.withinStore(pt) &&
                    (storeGrid.getTileOccupant(pt) == null || pt.equals(dest))){
                neighborNodes.add(new AStarData(pt, current));
            }
        }
        return neighborNodes;
    }

    private static AStarData findLowestFScore(LinkedList list){
        Comparator<AStarData> compareFScore = new Comparator<AStarData>() {
            @Override
            public int compare(AStarData lhs, AStarData rhs) {
                return lhs.fScore - rhs.fScore;
            }
        };

        LinkedList<AStarData> sortedList = (LinkedList)list.clone();
        Collections.sort(sortedList, compareFScore);

        return sortedList.getFirst();
    }

    public static class AStarData {
        private Point point;
        private int gScore;
        private int fScore;
        private AStarData from;

        public AStarData(Point pt, AStarData from){
            this.point = pt;
            this.from = from;
        }

        public AStarData(Point pt, int gScore){
            this.point = pt;
            this.from = null;
            this.gScore = gScore;
        }

        public Point getPoint(){
            return this.point;
        }

        @Override
        public boolean equals(Object that) {
            if (this == that)
                return true;
            else if(!(that instanceof AStarData))
                return false;
            return (this.point.equals((((AStarData) that).point)));
        }
    }
}

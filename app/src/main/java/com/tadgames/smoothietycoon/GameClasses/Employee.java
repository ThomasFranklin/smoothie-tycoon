package com.tadgames.smoothietycoon.GameClasses;

public class Employee {
    private String name; // Ex: Thomas, Dennis, etc.
    private String description; // Some random description

    private int ticksForRegister; // Less is better. Minimum of 2.
    private int ticksforSmoothieMaking; // Same as above.
    private int attitude; // More is better. Max of 10. Increases store appeal.

    private float nonPlayingRevenue; // Money generated per minute while not playing.

    private int level; // Employees will have a level that increases their abilities. The bonus will
        // be a linear function that maxes out at 1.5* bonus at lv 20.
    private int exp; // EXP for employees is 1-1 for each smoothie sold by the employee.
    private int expToNextLevel; // Some increasing function.

    private float cost;

    public Employee(String name, String description, int ticksForRegister,
                    int ticksforSmoothieMaking, int attitude, float nonPlayingRevenue, int level,
                    int exp, float cost){
        this.name = name;
        this.description = description;

        this.level = level;
        this.exp = exp;
        expToNextLevel = calculateExpToNextLevel();

        float levelMultiplier = calculateLevelBonus();

        this.ticksForRegister = (int) (ticksForRegister / levelMultiplier);
        this.ticksforSmoothieMaking = (int) (ticksforSmoothieMaking / levelMultiplier);
        this.attitude = (int) (attitude * levelMultiplier);

        this.nonPlayingRevenue = nonPlayingRevenue;
        this.cost = cost;
    }

    private int calculateExpToNextLevel(){
        return (level - 1) * 5 + 10;
    }

    private float calculateLevelBonus(){
        if (level < 20)
            return (float) ((level - 1) * .025 + 1);
        return (float) 1.5;
    }

    public float getNonPlayingRevenue(){
        return nonPlayingRevenue;
    }

    public float getCost(){
        return cost;
    }
}

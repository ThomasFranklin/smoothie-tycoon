package com.tadgames.smoothietycoon;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.RelativeLayout;

import com.tadgames.smoothietycoon.GameClasses.Point;
import com.tadgames.smoothietycoon.GameClasses.StoreObject;
import com.tadgames.smoothietycoon.databinding.PurchaseSuppliesScreenBinding;
import com.tadgames.smoothietycoon.databinding.ObjectPlacementOverlayBinding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SupplyScreenHandler {

    private static final String OBJ_LIB = "object_library.txt";
    private static final String DESC_LIB = "object_description_library.txt";

    private List<StoreObject> objectLibrary;

    private int firstItemIndex = 0;

    private GameActivity gameActivity;
    private MainGameView gameView;
    private Context gameContext;
    private View supplyScreen;
    private PurchaseSuppliesScreenBinding binding;
    private ObjectPlacementOverlayBinding placementBinding;
    private View objectPlacementOverlay;
    private ImageHandler imageHandler;

    private RelativeLayout placeObjectDialogue;

    private Point objectPlacementCoordinates;

    public SupplyScreenHandler(GameActivity gameActivity, MainGameView gameView, Context gameContext, View supplyScreen, View objectPlacementOverlay, ImageHandler imageHandler) {
        this.gameActivity = gameActivity;
        this.gameView = gameView;
        this.gameContext = gameContext;
        this.supplyScreen = supplyScreen;
        this.binding = new PurchaseSuppliesScreenBinding(supplyScreen);
        this.objectPlacementOverlay = objectPlacementOverlay;
        this.placementBinding = new ObjectPlacementOverlayBinding(objectPlacementOverlay);
        this.imageHandler = imageHandler;
        this.objectLibrary = new ArrayList<>();
        loadObjectLibrary();
        loadDescriptionLibrary();

        binding.setScreenHandler(this);
        placementBinding.setScreenHandler(this);

        placeObjectDialogue = (RelativeLayout) gameActivity.findViewById(R.id.PlaceLayout);
    }

    private void loadObjectLibrary(){
        List<String[]> objectsReadIn = FileManager.readGenericFile(OBJ_LIB, gameContext);
        for (String[] line: objectsReadIn){
            objectLibrary.add(new StoreObject(line[0], Integer.parseInt(line[2]), Integer.parseInt(line[3])));
        }
    }

    private void loadDescriptionLibrary(){
        HashMap<String, String> descriptionLibrary = new HashMap<>();
        List<String[]> descriptions = FileManager.readGenericFile(DESC_LIB, gameContext);
        for (String[] line: descriptions){
            descriptionLibrary.put(line[0], line[1]);
        }
        for (int i = 0 ; i < objectLibrary.size() ; i++){
            StoreObject object = objectLibrary.get(i);
            object.setDescription(descriptionLibrary.get(object.getName()));
        }
    }

    public void initializeScreen(){
        binding.setItem1(objectLibrary.get(0));
        binding.setItem2(objectLibrary.get(1));
        binding.setItem3(objectLibrary.get(2));
        binding.setItem4(objectLibrary.get(3));
        binding.setItem5(objectLibrary.get(4));
    }

    public void locationIsChosen(Point location){
        gameView.setPlaceObjectMode(false);
        objectPlacementCoordinates = location;
        placeObjectDialogue.setVisibility(View.VISIBLE);

    }

    public Drawable getDrawable(String name){
        return gameActivity.getDrawable(imageHandler.getStoreObjectID(name));
    }


    //OnClick Methods

    public void onClickBack(View v) {
        supplyScreen.setVisibility(View.GONE);
        gameActivity.overlay.setVisibility(View.VISIBLE);
    }

    public void onClickBuy(View v) {
        gameView.setPlaceObjectMode(true);
        supplyScreen.setVisibility(View.GONE);
        gameActivity.overlay.setVisibility(View.GONE);
        placeObjectDialogue.setVisibility(View.GONE);
        objectPlacementOverlay.setVisibility(View.VISIBLE);
    }

    public void onClickLeft(View v) {
        if (firstItemIndex >= 1){
            firstItemIndex--;
            binding.setItem5(binding.getItem4());
            binding.setItem4(binding.getItem3());
            binding.setItem3(binding.getItem2());
            binding.setItem2(binding.getItem1());
            binding.setItem1(objectLibrary.get(firstItemIndex));
        }
    }

    public void onClickRight(View v) {
        if ((firstItemIndex + 5) < objectLibrary.size()){
            firstItemIndex++;
            binding.setItem1(binding.getItem2());
            binding.setItem2(binding.getItem3());
            binding.setItem3(binding.getItem4());
            binding.setItem4(binding.getItem5());
            binding.setItem5(objectLibrary.get(firstItemIndex + 4));
        }
    }

    public void onClickItem1(View v) {
        binding.setSelectedItem(binding.getItem1());
    }

    public void onClickItem2(View v) {
        binding.setSelectedItem(binding.getItem2());
    }

    public void onClickItem3(View v) {
        binding.setSelectedItem(binding.getItem3());
    }

    public void onClickItem4(View v) {
        binding.setSelectedItem(binding.getItem4());
    }

    public void onClickItem5(View v) {
        binding.setSelectedItem(binding.getItem5());
    }

    public void onClickYes(View v) {
        gameView.placeObject(binding.getSelectedItem().copyObject(), objectPlacementCoordinates);
        gameView.setPlaceObjectMode(false);
        gameView.storeGrid.updateSelector(-10000, -10000); //Moves the selector off the map. I'll eventually just change it to a boolean somewhere
        placeObjectDialogue.setVisibility(View.GONE);
        objectPlacementOverlay.setVisibility(View.GONE);
        gameActivity.overlay.setVisibility(View.VISIBLE);
    }

    public void onClickNo(View v) {
        placeObjectDialogue.setVisibility(View.GONE);
        gameView.setPlaceObjectMode(true);
    }
}
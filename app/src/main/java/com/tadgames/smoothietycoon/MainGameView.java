package com.tadgames.smoothietycoon;


import android.content.Context;
import android.databinding.ObservableBoolean;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import com.tadgames.smoothietycoon.GameClasses.Employee;
import com.tadgames.smoothietycoon.GameClasses.Ingredient;
import com.tadgames.smoothietycoon.GameClasses.Point;
import com.tadgames.smoothietycoon.GameClasses.Recipe;
import com.tadgames.smoothietycoon.GameClasses.StoreObject;


public class MainGameView extends SurfaceView implements SurfaceHolder.Callback{

    private final static int tickRate = 500; // Milliseconds. 500 -> 2 ticks per second
    private final static int movementTickRate = 50;
    private long movementTime;
    private long nextTime;

    private final int storeRows = 46;
    private final int storeCols = 23;

    public MainGameThread gameThread;
    public StoreGrid storeGrid;
    private GameActivity game;
    private ImageHandler imageHandler;
    private SupplyScreenHandler supplyScreenHandler;

    private ArrayList<Pair<StoreObject, Point>> storeObjects;

    private PlayerProfile player;
    private LinkedList<Ingredient> ingredients;
    private CustomerHandler customerHandler;
    private ArrayList<Employee> employees;

    private boolean placeObjectMode;
    private ObservableBoolean recipeScreenOn;

    private long possibleDragStartTime;
    private boolean dragInProgress;
    private Point dragOrigin;
    private boolean firstMove;

    public MainGameView(Context context, GameActivity game, ImageHandler imageHandler, DisplayMetrics dm) {
        super(context);

        getHolder().addCallback(this);

        try {
            InputStream ingredientsList = context.getAssets().open("ingredient_library.txt");
            ingredients = Utility.loadIngredients(new Scanner(ingredientsList));
            ingredientsList.close();
        }
        catch (IOException e){
            System.out.println("Ingredients List Not Found");
            ingredients = null;
        }
        try{
            // Load Employees Placeholder
        }catch (Exception e){
            // Load Failed or First Time Playing. In either case, add 1 basic employee w/ bad stats
        }

        nextTime = System.currentTimeMillis() + tickRate;
        movementTime = System.currentTimeMillis() + movementTickRate;
        this.game = game;
        this.gameThread = new MainGameThread(getHolder(), this);
        this.imageHandler = imageHandler;
        this.dragInProgress = false;
        this.firstMove = true;
        this.placeObjectMode = false;
        recipeScreenOn = new ObservableBoolean(false);

        //Change these values later
        this.storeGrid = new StoreGrid(storeRows, storeCols, dm, imageHandler, game); //Second value MUST be odd, first value MUST be twice the second (Currently set to have the store 20 by 20 skewed tiles)
        player = new PlayerProfile(context); // SHOULD CHANGE THIS TO CHECK FOR PREVIOUSLY SAVED FILE!
        storeObjects = searchStoreGridForStoreObjects();

        customerHandler = new CustomerHandler(7, 10, 0, this, player, game, storeGrid, storeObjects);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                int xDown = (int)event.getX();
                int yDown = (int)event.getY();
                dragOrigin = new Point(xDown, yDown);
                possibleDragStartTime = System.currentTimeMillis();
                return true;
            case MotionEvent.ACTION_MOVE:
                if (System.currentTimeMillis() - possibleDragStartTime >= 200){
                    dragInProgress = true;
                    int xMove = (int)event.getX();
                    int yMove = (int)event.getY();
                    storeGrid.moveViewport(xMove, yMove, dragOrigin, firstMove);
                    firstMove = false;
                }
                return true;
            case MotionEvent.ACTION_UP:
                int xUp = (int)event.getX();
                int yUp = (int)event.getY();
                if (dragInProgress){
                    storeGrid.moveViewport(xUp, yUp, dragOrigin,  firstMove);
                    dragInProgress = false;
                    firstMove = true;
                }
                else if (placeObjectMode){
                    Point location = storeGrid.updateSelector(xUp, yUp);
                    if (storeGrid.locationIsValid(location)){
                        supplyScreenHandler.locationIsChosen(location);
                    }
                }
                return true;
        }
        return true;
    }

    public void Draw(Canvas canvas){
        if (canvas != null){
            //Draw stuffs here
            canvas.drawColor(Color.BLACK);
            storeGrid.drawStore(canvas);

            long time = System.currentTimeMillis();
            if (time >= nextTime){
                customerHandler.generateCustomer();
                customerHandler.handlePeopleInLine();
                customerHandler.serveCustomers();
                customerHandler.handleCustomersChillaxing();
                nextTime = time + tickRate;
            }
            if (time >= movementTime){
                customerHandler.handleMovingCustomers();
                movementTime = time + movementTickRate;
            }

            customerHandler.drawCustomers(canvas);
        }
    }
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        System.out.println("Surface created");
        gameThread.setRunning(true);
        gameThread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        while (retry) {
            try {
                gameThread.join();
                retry = false;
                System.out.println("Surface destroyed.");
            } catch (InterruptedException e) {
                System.out.println("Error destroying thread...attempting to retry...");
            }
        }
    }

    public void setSupplyScreenHandler(SupplyScreenHandler supplyScreenHandler) {
        this.supplyScreenHandler = supplyScreenHandler;
    }

    public LinkedList<Ingredient> getIngredients(){
        return ingredients;
    }

    public void addRecipe(Recipe recipe){
        player.addRecipe(recipe);
        customerHandler.updateHealthAppeal();
    }

    public void removeRecipe(Recipe recipe){
        player.removeRecipe(recipe);
        customerHandler.updateHealthAppeal();
    }

    public List<Recipe> getRecipes(){
        return player.getRecipes();
    }

    public PlayerProfile getPlayer(){
        return player;
    }

    public ArrayList<Employee> getEmployees(){
        return employees;
    }

    public void setPlaceObjectMode(boolean placeObjectMode) {
        this.placeObjectMode = placeObjectMode;
    }

    public void placeObject(StoreObject object, Point position){
        storeGrid.setTile(position, object);
        storeObjects = searchStoreGridForStoreObjects();
        customerHandler.updateStoreObjects();
    }

    public void updateOverlay(){
        game.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                View overlay = game.overlay;
                TextView playerCurMoney = (TextView) overlay.findViewById(R.id.playerCurrentMoney);
                playerCurMoney.setText("$" + player.getMoney());

                TextView playerLevel = (TextView) overlay.findViewById(R.id.playerLevel);
                playerLevel.setText(Integer.toString(player.getLevel()));

                ProgressBar playerExpBar = (ProgressBar) overlay.findViewById(R.id.playerExperience);
                playerExpBar.setMax(player.getExperienceToLevelUp());
                playerExpBar.setProgress(player.getExperience());
            }
        });
    }

    public ObservableBoolean isRecipeScreenOn(){
        return recipeScreenOn;
    }

    public void openRecipeScreen(){
        recipeScreenOn.set(true);
    }

    public void closeRecipeScreen(){
        recipeScreenOn.set(false);
    }

    private ArrayList<Pair<StoreObject, Point>> searchStoreGridForStoreObjects(){
        ArrayList<Pair<StoreObject,Point>> storeObjects = new ArrayList<>();
        for (int y = 0; y < storeRows; y++){
            for (int x = 0; x < storeCols; x++){
                StoreObject possibleStoreObj = storeGrid.getTileOccupant(x, y);
                if (possibleStoreObj != null)
                    storeObjects.add(new Pair<StoreObject, Point>(possibleStoreObj, new Point(x,y)));
            }
        }
        return storeObjects;
    }

    public ArrayList<Pair<StoreObject, Point>> getStoreObjects(){
        return storeObjects;
    }
}

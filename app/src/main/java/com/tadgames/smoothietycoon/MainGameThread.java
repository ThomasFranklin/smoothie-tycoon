package com.tadgames.smoothietycoon;


import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class MainGameThread extends Thread{

    private boolean running;
    private SurfaceHolder holder;
    private MainGameView gameView;

    public MainGameThread(SurfaceHolder holder, MainGameView gameView){
        this.running = false;
        this.holder = holder;
        this.gameView = gameView;
    }

    public void setRunning(boolean isRunning){
        this.running = isRunning;
    }

    @Override
    public void run() {
        Canvas canvas;

        while (running) {
            canvas = null;
            try{
                canvas = this.holder.lockCanvas();
                synchronized (holder) {
                    gameView.Draw(canvas);
                }
            }
            finally{
                if (canvas != null)
                    holder.unlockCanvasAndPost(canvas);
            }
        }
    }
}

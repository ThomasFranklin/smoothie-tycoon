package com.tadgames.smoothietycoon.GameClasses;


import java.io.Serializable;

public class StoreObject implements Serializable {

    //ALL fields must be serializable

    private String name;
    private String description;
    private int cost;
    private int unlockLevel; //Store level at which this item is unlocked and available to buy
    private int currentLevel; //Level of the item, this allows items to be upgraded


    public StoreObject(String name, int cost, int unlockLevel) {
        this.name = name;
        this.cost = cost;
        this.unlockLevel = unlockLevel;
        this.currentLevel = 1;
    }

    public StoreObject(String name, String description, int cost, int unlockLevel, int currentLevel){
        this.name = name;
        this.description = description;
        this.cost = cost;
        this.unlockLevel = unlockLevel;
        this.currentLevel = currentLevel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public StoreObject copyObject() {
        return new StoreObject(name, description, cost, unlockLevel, currentLevel);
    }

    public int getCost() {
        return cost;
    }

    public int getUnlockLevel() {
        return unlockLevel;
    }

    public int getCurrentLevel() {
        return currentLevel;
    }
}

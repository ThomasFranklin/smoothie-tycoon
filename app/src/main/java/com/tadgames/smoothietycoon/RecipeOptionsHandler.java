package com.tadgames.smoothietycoon;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tadgames.smoothietycoon.GameClasses.Recipe;

public class RecipeOptionsHandler {
    private Recipe recipe;
    private MainGameView gameView;
    private Context context;
    private RelativeLayout mainLayout;
    private RecipeListAdapter adapter;

    private float desiredPrice;

    private View recipeOptionsView;
    private View recipePricingView;

    public RecipeOptionsHandler(MainGameView gameView, RelativeLayout mainLayout,
                                RecipeListAdapter recipeListAdapter, Context context){
        this.gameView = gameView;
        this.mainLayout = mainLayout;
        this.context = context;
        adapter = recipeListAdapter;
    }

    private void inflateOptionsView(){
        LayoutInflater optionsInflater = (LayoutInflater.from(context));
        recipeOptionsView = optionsInflater.inflate(R.layout.recipe_options, null);

        Button modifyPriceButton = (Button) recipeOptionsView.findViewById(R.id.repriceRecipeButton);
        modifyPriceButton.setOnClickListener(modifyPricing);

        Button deleteRecipeButton = (Button) recipeOptionsView.findViewById(R.id.deleteRecipeButton);
        deleteRecipeButton.setOnClickListener(deleteRecipe);

        Button exitButton = (Button) recipeOptionsView.findViewById(R.id.exitRecipeOptions);
        exitButton.setOnClickListener(exitRecipeOptions);

        mainLayout.addView(recipeOptionsView);
        recipeOptionsView.setVisibility(View.VISIBLE);
    }

    private void updateRecipeInOptions(){
        TextView recipeName = (TextView) recipeOptionsView.findViewById(R.id.recipeOptionsTitle);
        if (recipe != null)
            recipeName.setText(recipe.getName());
        else
            recipeName.setText("Recipe was null");
    }

    View.OnClickListener modifyPricing = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (recipePricingView == null)
                inflatePricingScreen();
            else
                recipePricingView.setVisibility(View.VISIBLE);
        }
    };

    View.OnClickListener deleteRecipe = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            adapter.remove(recipe);
            exitRecipeOptions.onClick(null);
        }
    };

    View.OnClickListener exitRecipeOptions = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            recipe = null;
            recipeOptionsView.setVisibility(View.GONE);
        }
    };

    public void openRecipeOptions(Recipe recipe){
        this.recipe = recipe;
        desiredPrice = recipe.getPrice();

        if (recipeOptionsView == null)
            inflateOptionsView();

        recipeOptionsView.setVisibility(View.VISIBLE);
        updateRecipeInOptions();
    }

    private void inflatePricingScreen(){
        LayoutInflater layoutInf = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        recipePricingView = layoutInf.inflate(R.layout.smoothie_pricing_screen,null, false);
        mainLayout.addView(recipePricingView);

        recipePricingView.findViewById(R.id.increasePrice).setOnClickListener(increasePrice);
        recipePricingView.findViewById(R.id.lowerPrice).setOnClickListener(lowerPrice);
        recipePricingView.findViewById(R.id.cancelPricing).setOnClickListener(cancelPricing);
        recipePricingView.findViewById(R.id.finalizePricing).setOnClickListener(finalizePricing);

        recipePricingView.findViewById(R.id.clickedOffPricingScreen).setOnClickListener(cancelPricing);

        updatePriceTextView();

        recipePricingView.setVisibility(View.VISIBLE);
    }

    private void updatePriceTextView(){
        TextView price = (TextView) recipePricingView.findViewById(R.id.smoothiePricing);
        price.setText("$" + desiredPrice);
    }

    View.OnClickListener increasePrice = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            desiredPrice += 1;
            updatePriceTextView();
        }
    };

    View.OnClickListener lowerPrice = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (desiredPrice >= 1) {
                desiredPrice -= 1;
                updatePriceTextView();
            }
        }
    };

    View.OnClickListener cancelPricing = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            updatePriceTextView();
            recipePricingView.setVisibility(View.GONE);
        }
    };

    View.OnClickListener finalizePricing = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            recipe.setPrice(desiredPrice);
            recipePricingView.setVisibility(View.GONE);
        }
    };
}

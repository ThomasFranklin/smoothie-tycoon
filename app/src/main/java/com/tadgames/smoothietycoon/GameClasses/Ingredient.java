package com.tadgames.smoothietycoon.GameClasses;

import java.io.Serializable;

public class Ingredient implements Serializable{
    private int size; // Relative Scale. A strawberry is of size 3, Blueberry 1.
    // Fractional Ingredient Adding will be handled elsewhere!!!!

    private Flavor flavor;

    private int calories; // Yeah just pull this data from some science-y thing.
    private int healthiness; // Mix of real and public perception of ingredient healthiness

    private String name; // Also used as a key for images
    private String colorContrib;

    private float cost;

    private boolean unlocked; // Is the ingredient unlocked/discovered?

    public Ingredient(String name, int size, int sweetness, int tartness, int spiciness,
                      int bitterness, int sourness, int nuttiness, int saltiness, int calories,
                      int healthiness, String colorContrib, float cost){
        this.name = name;
        this.size = size;

        this.flavor = new Flavor(sweetness, tartness, spiciness, bitterness, sourness, nuttiness,
                saltiness);

        this.calories = calories;
        this.healthiness = healthiness;
        this.colorContrib = colorContrib;

        this.cost = cost;

        this.unlocked = true; // When game is loading all ingredients will be checked against
        // the savefile to see if they are unlocked or not.
    }

    public int getSize() {
        return size;
    }

    public Flavor getFlavor(){
        return flavor;
    }

    public int getCalories(){
        return calories;
    }

    public int getHealthiness() {
        return healthiness;
    }

    public String getName() {
        return name;
    }

    public String getColorContrib() {
        return colorContrib;
    }

    public boolean getUnlocked(){
        return unlocked;
    }

    public void setUnlockedStatus(boolean b){
        unlocked = b;
    }

    public float getCost(){
        return cost;
    }
}

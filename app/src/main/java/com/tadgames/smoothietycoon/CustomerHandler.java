package com.tadgames.smoothietycoon;

import android.graphics.Canvas;
import android.util.Pair;

import com.tadgames.smoothietycoon.GameClasses.Customer;
import com.tadgames.smoothietycoon.GameClasses.Employee;
import com.tadgames.smoothietycoon.GameClasses.Flavor;
import com.tadgames.smoothietycoon.GameClasses.Point;
import com.tadgames.smoothietycoon.GameClasses.Recipe;
import com.tadgames.smoothietycoon.GameClasses.StoreObject;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CustomerHandler {
    private static final int TICKSTOMOVEONESQUARE = 20;
    private static final float STEPINTERVALS =  1 / ((float)TICKSTOMOVEONESQUARE); // Used to move things between points

    private static final Point LINEORIGIN = new Point (8, 16);
    private static final Point FIRST_SERVING_STATION_LOC = new Point (9,8);
    private static final Point SECOND_SERVING_STATION_LOC = new Point (10,10);

    private LinkedList<Customer> customersInLine;
    private ArrayList<Customer> customersServing;
    private LinkedList<Customer> customersChillaxing;
    private LinkedList<Customer> movingCustomers;

    private int lineCapacity;
    private int customerServingCapacity;
    private int customerSittingCapacity;

    private int healthAppeal; // THE AVERAGE HEALTH RATING OF SMOOTHIES
    private int tasteAppeal; // THIS IS TOTALLY NOT USED RIGHT NOW.... NOT SURE WHY IT EXISTS
    private int companyAppeal; // So like do we do cool charitable stuff or source from local
        // organic thingy mahoo~kies. PROBABLY WILL INCREASE WITH ADVERTISING OR SOMETHING

    private MainGameView gameView;
    private GameActivity game;
    private PlayerProfile player;
    private StoreGrid storeGrid;
    private ArrayList<Pair<StoreObject, Point>> storeObjects;
    private LinkedList<Point> cashRegisterLocations = new LinkedList<>(); // Used for customer pathing.

    private ArrayList<Employee> employees;

    private int xConstantDrawing;
    private int xOffsetForOddY;
    private int yConstantDrawing;
    private ImageHandler imageHandler;

    public CustomerHandler(int lineCapacity, int customerSittingCapacity, int companyAppeal,
                           MainGameView gameView, PlayerProfile player, GameActivity game,
                           StoreGrid storeGrid, ArrayList<Pair<StoreObject, Point>> storeObjects){

        this.gameView = gameView;
        this.game = game;
        this.storeGrid = storeGrid;
        employees = gameView.getEmployees();
        this.storeObjects = storeObjects;

        initializeCashRegisters();

        this.lineCapacity = lineCapacity;
        customerServingCapacity = cashRegisterLocations.size();
        this.customerSittingCapacity = customerSittingCapacity;
        this.companyAppeal = companyAppeal;

        customersInLine = new LinkedList<>();
        customersServing = new ArrayList<>();
        for (int i = 0; i < customerServingCapacity; i ++){
            customersServing.add(null);
        }
        customersChillaxing = new LinkedList<>();
        movingCustomers = new LinkedList<>();

        healthAppeal = calculateHealthAppeal();
        this.player = player;

        imageHandler = storeGrid.getImageHandler();
        xConstantDrawing = imageHandler.getTileWidth();
        xOffsetForOddY = xConstantDrawing / 2;
        yConstantDrawing = imageHandler.getTileHeight() / 2;
    }

    private int calculateHealthAppeal(){
        int totalHealthRating = 0;
        int totalRecipes = 0;
        for (Recipe r : gameView.getRecipes()){
            totalHealthRating += r.getHealthiness();
            totalRecipes++;
        }
        if (totalRecipes > 0)
            return totalHealthRating / totalRecipes;

        return 0;
    }

    public void updateHealthAppeal(){
        healthAppeal = calculateHealthAppeal();
    }

    public void generateCustomer(){
        if (customersInLine.size() < lineCapacity) {
            int rando = (int) (Math.random() * 16);
            Customer newCustomer;

            if (rando <= healthAppeal + companyAppeal) {
                newCustomer = new Customer(4, 3, 0, 0, 2, 0, 0, 10, 300, "healthy", 10,
                        LINEORIGIN);
            } else {
                newCustomer = new Customer(7, 3, 0, 0, 3, 0, 0, 4, 500, "regular", 7,
                        LINEORIGIN);
            }

            player.upCustomersVisited();
            customerCreationBackend(newCustomer);

            double anotherCustRando = Math.random();
            if (anotherCustRando < .25)
                generateCustomer();
        }
    }

    private void customerCreationBackend(Customer customer){
        customersInLine.addLast(customer);
        int spotInLine = customersInLine.indexOf(customer);

        if (spotInLine == 0 && isAServingStationOpen()){
            transferFromLineToAppropriateServingStation(customer);
        }
        else {
            customer.setPosition(new Point(LINEORIGIN.x, LINEORIGIN.y + spotInLine));
        }
    }

    public void handlePeopleInLine(){

        while (customersInLine.size() > 0 && isAServingStationOpen()) {
            Customer firstCustomerInLine = customersInLine.get(0);
            transferFromLineToAppropriateServingStation(firstCustomerInLine);
        }
        int i = 0;
        for (Customer c : customersInLine){
            c.upTicksInLine();
            c.setPosition(new Point (LINEORIGIN.x, LINEORIGIN.y + i));
            i++;
        }
    }

    private boolean isAServingStationOpen(){
        for (int i = 0; i < customerServingCapacity; i++){
            if (customersServing.get(i) == null)
                return true;
        }
        return false;
    }

    private void transferFromLineToAppropriateServingStation(Customer customer){
        customersInLine.remove(customer);
        int servingStation = 0;

        if (customer.getTicksInLine() >= 10)
            player.getFrownyFace();

        for (int i = 0; i < customerServingCapacity; i++){
            if (customersServing.get(i) == null){
                servingStation = i;
                break;
            }
        }

        customersServing.set(servingStation, customer);
        customer.destination = cashRegisterLocations.get(servingStation);
        movingCustomers.add(customer);
    }

    // THIS METHOD SHOULD ONLY BE CALLED ON EACH "TICK" OF THE GAME
    // We must determine how fast we want that. There is also no time or animation for this yet
        // so customers are served instantly.................... which is great service eh?
    public void serveCustomers(){
        int i = 0;
        for (Customer c : customersServing){
            if (c != null) {
                if (cashRegisterLocations.contains(c.getPosition())) {
                    Recipe smoothieToBuy = findBestSmoothie(c);

                    if (smoothieToBuy == null) {
                        player.getFrownyFace();
                        customersServing.set(i, null);
                        // ANIMATE CUSTOMER WALKING OUT OF STORE ANGRILY BECAUSE WE HAVE
                        // OVERPRICED SMOOTHIES!!!!
                    } else {
                        player.soldSmoothie(smoothieToBuy.getPrice());
                        smoothieToBuy.smoothieWasSold();
                        game.getProfileHandler().updateSoldRecipes();
                        gameView.updateOverlay();

                        customersServing.set(i, null);

                        if (customersChillaxing.size() < customerSittingCapacity) {
                            player.getHappyFace();
                            customersChillaxing.addLast(c);
                            c.setTicksToFinishDrinking(10);
                        } else {
                            player.getFrownyFace();
                            // DRAW CUSTOMER BEING ANNOYED THERE'S NO SITTING ROOM FOR HIM!
                        }
                    }
                }
            }
            i++;
        }
    }

    public void handleCustomersChillaxing(){
        LinkedList<Customer> customersToRemove = new LinkedList<>();
        for (Customer c : customersChillaxing){
            if (c.getTicksToFinishDrinking() == 0){
                customersToRemove.add(c);
            }
            c.upTicksDrinking();
        }
        for (Customer c: customersToRemove){
            customersChillaxing.remove(c);
        }
    }

    private Recipe findBestSmoothie(Customer customer){
        // THIS COPY IS SO THAT IF WE ARE CHANGING RECIPES AT THE SAME TIME WE DO NOT GET SOME
        // CRAZY ERRORS
        List<Recipe> menu = new LinkedList<>(gameView.getRecipes());
        float budget = customer.getMaxBudget();

        if (menu.size() > 0) {
            if (customer.getName().equals("healthy")) {
                Recipe healthiestRecipe = menu.get(0);

                for (Recipe r : menu) {
                    if (r.getHealthiness() > healthiestRecipe.getHealthiness() &&
                            r.getPrice() <= budget) {
                        healthiestRecipe = r;
                    }
                }

                if (healthiestRecipe.getPrice() <= budget) {
                    player.getHappyFace();
                    return healthiestRecipe;
                }
                // SENTINEL VALUE SAYING THAT NO AFFORDABLE SMOOTHIE WAS FOUND;
                return null;
            } else if (customer.getName().equals("regular")) {
                int closestMatch = 1000;
                Recipe bestRecipe = menu.get(0);
                Flavor tastes = customer.getTastes();

                for (Recipe r : menu) {
                    int tasteDiff = Flavor.compareFlavors(tastes, r.getSmoothieFlavor());
                    if (tasteDiff < closestMatch && r.getPrice() <= budget) {
                        bestRecipe = r;
                        closestMatch = tasteDiff;
                    }
                }

                if (bestRecipe.getPrice() <= budget) {
                    player.getHappyFace();
                    return bestRecipe;
                }

                // SENTINEL VALUE SAYING THAT NO AFFORDABLE SMOOTHIE WAS FOUND
                return null;
            }
        }
        else {
            return null;
        }

        // SENTINEL VALUE SAYING THAT MAN WE HAVE AN INCORRECT CUSTOMER NAME!!!!!!
        throw new InvalidParameterException("The name of the customer is not currently handled");
    }

    public void drawCustomers(Canvas canvas){
        for (Customer c : customersInLine){
            drawSingleCustomer(c, canvas);
        }
        for (Customer c : customersServing){
            if (c != null && !(movingCustomers.contains(c)))
                drawSingleCustomer(c, canvas);
        }
        for (Customer c : customersChillaxing){
            drawSingleCustomer(c, canvas);
        }
        drawMovingCustomers(canvas);
    }

    private void drawSingleCustomer(Customer c, Canvas canvas){
        Point viewportOrigin = storeGrid.getViewportOrigin();
        int xDraw = xOffsetForOddY;
        /*if (c.getPosition().y % 2 != 0)
            xDraw = xOffsetForOddY;
        else
            xDraw = 0;*/

        canvas.drawBitmap(imageHandler.getCustomerImage(c.getName()),
                xConstantDrawing * c.getPosition().x + viewportOrigin.x + xDraw,
                yConstantDrawing * c.getPosition().y + viewportOrigin.y, null);
    }

    public void handleMovingCustomers(){
        LinkedList<Customer> customersToRemove = new LinkedList<>();
        for (Customer c : movingCustomers){
            if (c.movementTick == 0) {
                Point nextPt = Utility.aStarToNextPos(c, storeGrid);
                c.nextPt = nextPt;
                if (nextPt.x > c.getPosition().x)
                    c.direction = Customer.RIGHT;
                else if (nextPt.x < c.getPosition().x)
                    c.direction = Customer.LEFT;
                else if (nextPt.y < c.getPosition().y)
                    c.direction = Customer.UP;
                else
                    c.direction = Customer.DOWN;
                c.movementTick++;
            }

            else if (c.movementTick == TICKSTOMOVEONESQUARE){
                c.setPosition(c.nextPt);
                if (c.getPosition().equals(c.destination)) {
                    customersToRemove.add(c);
                }
                c.movementTick = 0;
            }
            else {
                c.movementTick++;
            }
        }
        for (Customer c : customersToRemove){
            movingCustomers.remove(c);
            c.destination = null;
        }
    }

    private void drawMovingCustomers(Canvas canvas){
        Point viewportOrigin = storeGrid.getViewportOrigin();
        int xDraw = xOffsetForOddY;

        for (Customer c : movingCustomers) {
            /*if (c.getPosition().y % 2 != 0)
                xDraw = xOffsetForOddY;
            else
                xDraw = 0;*/

            if (c.direction == Customer.RIGHT)
                canvas.drawBitmap(imageHandler.getCustomerImage(c.getName()),
                        xConstantDrawing * (c.getPosition().x + STEPINTERVALS * c.movementTick)
                                + viewportOrigin.x + xDraw, yConstantDrawing * c.getPosition().y + viewportOrigin.y, null);
            else if (c.direction == Customer.LEFT)
                canvas.drawBitmap(imageHandler.getCustomerImage(c.getName()), xConstantDrawing *
                        (c.getPosition().x + STEPINTERVALS * c.movementTick * -1)
                        + viewportOrigin.x + xDraw, yConstantDrawing * c.getPosition().y + viewportOrigin.y, null);
            else if (c.direction == Customer.DOWN)
                canvas.drawBitmap(imageHandler.getCustomerImage(c.getName()),
                        xConstantDrawing * c.getPosition().x + viewportOrigin.x + xDraw,
                        yConstantDrawing * (c.getPosition().y + STEPINTERVALS * c.movementTick)
                                + viewportOrigin.y, null);
            else
                canvas.drawBitmap(imageHandler.getCustomerImage(c.getName()),
                        xConstantDrawing * c.getPosition().x + viewportOrigin.x + xDraw,
                        yConstantDrawing * (c.getPosition().y + STEPINTERVALS * c.movementTick * -1)
                                + viewportOrigin.y, null);
        }
    }

    private void initializeCashRegisters(){
        for (Pair<StoreObject, Point> storeObjectPointPair : storeObjects){
            if (storeObjectPointPair.first.getName().equals("Cash Register")){
                Point p = storeObjectPointPair.second;
                int x = p.x - 1;
                int y = p.y - 2;
                cashRegisterLocations.add(new Point(x, y));
            }
        }
    }

    public void updateStoreObjects(){
        this.storeObjects = gameView.getStoreObjects();
        cashRegisterLocations.clear();
        initializeCashRegisters();
        int numCashRegisters = cashRegisterLocations.size();
        if (numCashRegisters > customerServingCapacity){
            int diff = numCashRegisters - customerServingCapacity;
            for (int i = 0; i < diff; i++){
                customersServing.add(null);
            }
            customerServingCapacity = numCashRegisters;
        }
    }

}

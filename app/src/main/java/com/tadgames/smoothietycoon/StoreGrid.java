package com.tadgames.smoothietycoon;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.DisplayMetrics;

import com.tadgames.smoothietycoon.GameClasses.Point;
import com.tadgames.smoothietycoon.GameClasses.StoreObject;

public class StoreGrid {

    private String[][] backgroundTiles;
    private String[][] walls;
    private StoreObject[][] tiles;
    private Point[][][] positions;
    private int rows;
    private int cols;
    private Point viewportOrigin;
    private ImageHandler imageHandler;
    private int imageWidth;
    private int imageHeight;
    private Point lastMoveCoordinates;
    private int clampXMax;
    private int clampXMin;
    private int clampYMax;
    private int clampYMin;
    private Point selector;

    public StoreGrid(int rows, int cols, DisplayMetrics dm, ImageHandler imageHandler, Context context) {
        this.rows = rows;
        this.cols = cols;
        this.imageHandler = imageHandler;
        this.imageWidth = imageHandler.getTileWidth();
        this.imageHeight = imageHandler.getTileHeight();
        this.viewportOrigin = new Point(- 2 * imageWidth, - imageHeight);
        this.lastMoveCoordinates = new Point(0, 0);
        this.clampXMax = - 2 * imageWidth;
        this.clampXMin = - (cols - (dm.widthPixels / imageWidth)) * imageWidth + (2 * imageWidth);
        this.clampYMax = - imageHeight;
        this.clampYMin = - ((rows / 2) - (dm.heightPixels / imageHeight)) * imageHeight + (int)(1.5 * imageHeight);
        this.selector = new Point(0,0);
        initializePositions();
        this.backgroundTiles = FileManager.initializeBackground(context, rows, cols);
        this.walls = FileManager.initializeWalls(context, rows, cols);
        System.out.println("Background Set");
        this.tiles = new StoreObject[rows][];
        for(int yCoord = 0 ; yCoord < rows ; yCoord++){
            tiles[yCoord] = new StoreObject[cols];
            for(int xCoord = 0 ; xCoord < cols ; xCoord++){
                if ((yCoord == 10 && xCoord == 10) /*|| (yCoord == 12 && xCoord == 11)*/){
                    tiles[yCoord][xCoord] = new StoreObject("Cash Register", 0, 0);
                }
                else {
                    tiles[yCoord][xCoord] = null;
                }
            }
        }
    }

    private void initializePositions(){
        int xConstant = (imageWidth / 2);
        int yConstant = ((imageHeight / 2));

        this.positions = new Point[rows][][];
        for(int i = 0 ; i < rows ; i++){
            positions[i] = new Point[cols][];
            for(int j = 0 ; j < cols ; j++) {
                Point[] posns = new Point[3];
                int x;
                if (i % 2 != 0){
                    x = imageWidth*j + xConstant;
                }
                else {
                    x = imageWidth*j;
                }
                int y0 = yConstant*i;
                int y1 = y0  - imageHandler.getWallHeight() + imageHeight;
                int y2 = y0 - imageHandler.getObjectHeight() + imageHeight;
                posns[0] = new Point(x, y0);
                posns[1] = new Point(x, y1);
                posns[2] = new Point(x, y2);
                positions[i][j] = posns;
            }
        }
    }

    public Point getTilePosition(Point gridTile){
        return positions[gridTile.y][gridTile.x][0];
    }

    public Point updateSelector(int x, int y){
        Point tile = getClosestTile(x, y);
        selector.x = tile.x;
        selector.y = tile.y;
        return tile;
    }

    private Point getClosestTile(int x, int y){
        int realX = x - viewportOrigin.x;
        int realY = y - viewportOrigin.y;
        Point closest = new Point(0, 0);
        double dist = distanceFrom(new Point(imageWidth / 2, imageHeight / 2), realX, realY);
        for (int i = 0 ; i < rows ; i++) {
            for (int j = 0; j < cols; j++) {
                Point tile =  positions[i][j][0];
                Point center = new Point(tile.x + (imageWidth / 2), tile.y + (imageHeight / 2));
                double newDist = distanceFrom(center, realX, realY);
                if (newDist <= dist){
                    dist = newDist;
                    closest = new Point(i, j);
                }
            }
        }
        return closest;
    }

    private double distanceFrom(Point center, int x, int y){
        return Math.sqrt(Math.pow(center.x - x, 2) + Math.pow((center.y - y) / 2, 2));
    }

    public void setTile(Point point, StoreObject occupant){
        tiles[point.x][point.y] = occupant;
    }

    public StoreObject getTileOccupant(Point point){
        return tiles[point.y][point.x];
    }

    public StoreObject getTileOccupant(int x, int y){
        return tiles[y][x];
    }

    public Point getViewportOrigin(){
        return viewportOrigin;
    }

    public ImageHandler getImageHandler(){
        return imageHandler;
    }

    public boolean withinStore(Point point){
        return point.x >= 0 && point.x < cols && point.y >= 0 && point.y < rows;
    }

    public boolean locationIsValid(Point point){
        return (!backgroundTiles[point.x][point.y].equals("grass"))&&(tiles[point.x][point.y] == null);
    }

    public void moveViewport(int newX, int newY, Point dragOrigin, boolean firstMove){
        int deltaX;
        int deltaY;
        if (firstMove){
            deltaX = newX - dragOrigin.x;
            deltaY = newY - dragOrigin.y;
        }
        else {
            deltaX = newX - lastMoveCoordinates.x;
            deltaY = newY - lastMoveCoordinates.y;
        }
        lastMoveCoordinates = new Point(newX, newY);
        int viewportX = Utility.clamp(clampXMax, clampXMin, viewportOrigin.x + deltaX);
        int viewportY = Utility.clamp(clampYMax, clampYMin, viewportOrigin.y + deltaY);
        viewportOrigin = new Point(viewportX, viewportY);
    }

    public void drawStore(Canvas canvas){
        //Draws solid color onto the screen first to eliminate the black dots.
        canvas.drawRGB(109, 190, 69);
        for (int i = 0 ; i < rows ; i++) {
            for (int j = 0; j < cols; j++) {
                String img = backgroundTiles[i][j];
                if (img.equals("tile")){
                    Point tilePosn = positions[i][j][0];
                    Rect backdrop = new Rect(tilePosn.x + viewportOrigin.x,
                            tilePosn.y + viewportOrigin.y,
                            tilePosn.x + imageWidth + viewportOrigin.x,
                            tilePosn.y + imageHeight + viewportOrigin.y);
                    Paint paint = new Paint();
                    paint.setARGB(255, 241, 99, 35);
                    canvas.drawRect(backdrop, paint);
                }
            }
        }
        for (int i = 0 ; i < rows ; i++){
            for (int j = 0 ; j < cols ; j++){
                String img = backgroundTiles[i][j];
                String wall = walls[i][j];
                Point tilePosn = positions[i][j][0];
                Point wallPosn = positions[i][j][1];
                canvas.drawBitmap(imageHandler.getBackgroundImage(img),
                        tilePosn.x + viewportOrigin.x, tilePosn.y + viewportOrigin.y, null);
                if (wall != null){
                    canvas.drawBitmap(imageHandler.getWallImage(wall),
                            wallPosn.x + viewportOrigin.x,
                            wallPosn.y + viewportOrigin.y, null);
                }
            }
        }

        Point selectorPosn = positions[selector.x][selector.y][0];
        canvas.drawBitmap(imageHandler.getBackgroundImage("selector"), selectorPosn.x + viewportOrigin.x, selectorPosn.y + viewportOrigin.y, null);

        for(int i = 0 ; i < rows ; i++){
            for(int j = 0 ; j < cols ; j++){
                if (tiles[i][j] != null){
                    Point objectPosn = positions[i][j][2];
                    canvas.drawBitmap(imageHandler.getStoreObjectImage(tiles[i][j].getName()),
                            objectPosn.x + viewportOrigin.x, objectPosn.y + viewportOrigin.y, null);
                }
            }
        }
    }

    public void saveStore(Context context){
        SaveGameHandler.saveStoreGrid(context, tiles);
    }

    public void loadStore(Context context){
        tiles = SaveGameHandler.loadStoreGrid(context);
    }
}

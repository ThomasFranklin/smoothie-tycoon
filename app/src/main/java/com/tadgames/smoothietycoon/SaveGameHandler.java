package com.tadgames.smoothietycoon;


import android.content.Context;

import com.tadgames.smoothietycoon.GameClasses.StoreObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SaveGameHandler {

    private static final String STORE_GRID_FILENAME = "saved_store_grid.dat";

    public static void saveStoreGrid(Context context, StoreObject[][] grid){
        File gridFile = new File(context.getFilesDir(), STORE_GRID_FILENAME);
        try{
            FileOutputStream fOut = new FileOutputStream(gridFile);
            ObjectOutputStream oOut = new ObjectOutputStream(fOut);
            oOut.writeObject(grid);
            oOut.close();
            fOut.close();
        }
        catch (IOException e){
            System.out.println("Grid file could not be written");
            e.printStackTrace();
        }
    }

    public static StoreObject[][] loadStoreGrid(Context context){
        StoreObject[][] grid = null;
        File gridFile = new File(context.getFilesDir(), STORE_GRID_FILENAME);
        try{
            FileInputStream fIn = new FileInputStream(gridFile);
            ObjectInputStream oIn = new ObjectInputStream(fIn);
            grid = (StoreObject[][]) oIn.readObject();
            oIn.close();
            fIn.close();
        }
        catch (IOException e){
            System.out.println("Saved data could not be read");
            e.printStackTrace();
        }
        catch (ClassNotFoundException e){
            e.printStackTrace();
        }
        deleteSavedGame(context);
        return grid;
    }

    public static boolean checkIfSaveExists(Context context){
        File file = new File(context.getFilesDir(), STORE_GRID_FILENAME);
        return file.exists();
    }

    public static void deleteSavedGame(Context context){
        if (checkIfSaveExists(context)){
            File file = new File(context.getFilesDir(), STORE_GRID_FILENAME);
            file.delete();
        }
    }


}

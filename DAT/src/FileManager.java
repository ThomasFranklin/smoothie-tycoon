import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileManager {

    private static final String PATH = "D:\\Documents\\AndroidStudioProjects\\SmoothieTycoon\\DAT\\src\\";

    public static List<String> loadBackgroundOptions(){
        List<String> options = new ArrayList<>();
        options.add("grass");
        options.add("tile");
        return options;
    }

    public static List<String> loadWallOptions(){
        List<String> walls = new ArrayList<>();
        Scanner inputFile;
        try {
            inputFile = new Scanner(new FileInputStream(PATH + "wall_library.txt"));
        }
        catch (IOException ioe){
            ioe.printStackTrace();
            System.err.print("FILE COULD NOT BE READ");
            return walls;
        }
        while (inputFile.hasNextLine()){
            walls.add(inputFile.nextLine());
        }
        return walls;
    }

    public static String[][] initializeBackground(int ROWS, int COLS){
        String[][] background;
        File file = new File(PATH + "default_background.txt");
        if (file.exists()){
            System.out.println("Saved file found");
            background = loadFile(file, ROWS, COLS);
        }
        else {
            background = new String[ROWS][];
            for(int ithRow = 0 ; ithRow < ROWS ; ithRow++){
                background[ithRow] = new String[COLS];
                for(int jthCol = 0 ; jthCol < COLS ; jthCol++){
                    if ((ithRow + (2 * jthCol) >= (ROWS/ 2) + 6)
                            &&((2 * jthCol) - ithRow <= (ROWS / 2) - 9)
                            &&((2 * jthCol) - ithRow  >= (- ROWS / 2) + 4)
                            &&(ithRow + (2 * jthCol) <= ((3 * ROWS)/ 2) - 7)){
                        background[ithRow][jthCol] = "tile";
                    }
                    else{
                        background[ithRow][jthCol] = "grass";
                    }
                }
            }
        }
        return background;
    }

    public static String[][] initializeWalls(int ROWS, int COLS){
        String[][] walls;
        File file = new File(PATH + "default_walls.txt");
        if (file.exists()){
            walls = loadFile(file, ROWS, COLS);
        }
        else {
            walls = new String[ROWS][];
            for(int ithRow = 0 ; ithRow < ROWS ; ithRow++) {
                walls[ithRow] = new String[COLS];
                for (int jthCol = 0; jthCol < COLS; jthCol++) {
                    walls[ithRow][jthCol] = null;
                }
            }
        }
        return walls;
    }

    private static String[][] loadFile(File file, int ROWS, int COLS){
        String[][] gridData = new String[ROWS][];
        FileInputStream inputStream;
        try {
            inputStream = new FileInputStream(file);
        }
        catch (FileNotFoundException e){
            System.out.println("File could not be found");
            return gridData;
        }
        Scanner input = new Scanner(inputStream);
        for(int ithRow = 0 ; ithRow < ROWS ; ithRow++) {
            gridData[ithRow] = new String[COLS];
            for (int jthCol = 0; jthCol < COLS; jthCol++) {
                if (input.hasNextLine()){
                    String line = input.nextLine();
                    if (line.equals("null")){
                        gridData[ithRow][jthCol] = null;
                    }
                    else {
                        gridData[ithRow][jthCol] = line;
                    }
                }
                else {
                    System.out.println("File formatted incorrectly");
                }
            }
        }
        input.close();
        try {
            inputStream.close();
        }
        catch (IOException e){
            System.out.println("File could not be closed");
        }
        return gridData;
    }

    public static void saveBackground(String[][] backgroundTiles, int rows, int cols){
        File file = new File(PATH + "default_background.txt");
        if (file.exists()){
            file.delete();
        }
        PrintWriter out;
        try {
            out = new PrintWriter(file);
        }
        catch (FileNotFoundException e){
            System.out.println("Background save file could not be written");
            return;
        }
        writeToFile(out, backgroundTiles, rows, cols);
        out.close();
        System.out.println("Background successfully saved");
    }

    public static void saveWalls(String[][] walls, int rows, int cols){
        File file = new File(PATH + "default_walls.txt");
        if (file.exists()){
            file.delete();
        }
        PrintWriter out;
        try {
            out = new PrintWriter(file);
        }
        catch (FileNotFoundException e){
            System.out.println("Walls save file could not be written");
            return;
        }
        writeToFile(out, walls, rows, cols);
        out.close();
        System.out.println("Walls successfully saved");
    }

    private static void writeToFile(PrintWriter out, String[][] gridData, int rows, int cols){
        for (int i = 0 ; i < rows ; i++) {
            for (int j = 0; j < cols; j++) {
                if (gridData[i][j] == null){
                    out.println("null");
                }
                else{
                    out.println(gridData[i][j]);
                }
            }
        }
    }

    public static void deleteFiles(){
        File backgroundFile = new File(PATH + "default_background.txt");
        if (backgroundFile.exists()) {
            backgroundFile.delete();
        }
        File wallsFile = new File(PATH + "default_walls.txt");
        if (wallsFile.exists()){
            wallsFile.delete();
        }
    }
}

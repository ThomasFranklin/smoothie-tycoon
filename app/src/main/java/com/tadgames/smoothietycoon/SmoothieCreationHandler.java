package com.tadgames.smoothietycoon;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tadgames.smoothietycoon.GameClasses.Ingredient;
import com.tadgames.smoothietycoon.GameClasses.Recipe;

import org.w3c.dom.Text;

import java.util.LinkedList;
import java.util.List;

public class SmoothieCreationHandler {

    private Recipe tempRecipe;

    private View creationScreen;
    private View smoothiePricing;
    private MainGameView gameView;
    private Context context;
    private RelativeLayout mainLayout;

    private int blenderSize;
    private int blenderFilled;
    RecipeListAdapter recipeListAdapter;
    public SmoothieCreationHandler(Context context, View creationScreen, MainGameView gameView,
                                   RelativeLayout mainLayout, int blenderSize,
                                   RecipeListAdapter recipeListAdapter){
        tempRecipe = new Recipe();
        this.context = context;
        this.creationScreen = creationScreen;
        this.mainLayout = mainLayout;

        this.gameView = gameView;
        this.blenderSize = blenderSize;
        this.blenderFilled = 0;

        ListView ingListView = (ListView) creationScreen.findViewById(R.id.ingredientListView);
        ingListView.setOnItemClickListener(ingredientClickListener);

        Button saveRecipe = (Button) creationScreen.findViewById(R.id.createUsingIngredients);
        saveRecipe.setOnClickListener(saveSmoothieRecipe);

        this.recipeListAdapter = recipeListAdapter;
    }

    AdapterView.OnItemClickListener ingredientClickListener = new AdapterView.OnItemClickListener(){
        @Override
        public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
            final Ingredient ingredient = (Ingredient) parent.getItemAtPosition(position);
            final int ingredientSize = ingredient.getSize();

            if (ingredientSize + blenderFilled <= blenderSize) {
                tempRecipe.addIngredient(ingredient);
                blenderFilled += ingredientSize;

                final LinearLayout recipeLayout = (LinearLayout) creationScreen.findViewById(R.id.recipe);
                ImageView newRecipeIngredientImage = new ImageView(context);
                newRecipeIngredientImage.setImageResource(R.mipmap.strawberry);
                recipeLayout.addView(newRecipeIngredientImage, 0);

                updateCost();

                newRecipeIngredientImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        tempRecipe.removeIngredient(ingredient);
                        blenderFilled -= ingredientSize;
                        recipeLayout.removeView(v);
                        updateCost();
                    }
                });
            }
            else {
                // Somehow provide a message saying blender cannot hold more.
            }
        }
    };

    public void updateCost() {
        TextView cost = (TextView) creationScreen.findViewById(R.id.smoothieCost);
        cost.setText("Cost: " + tempRecipe.getCost().toString());
    }

    View.OnClickListener saveSmoothieRecipe = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (smoothiePricing == null)
                inflatePricingScreen();
            else{
                smoothiePricing.setVisibility(View.VISIBLE);
            }
        }
    };

    private void inflatePricingScreen(){
        LayoutInflater layoutInf = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        smoothiePricing = layoutInf.inflate(R.layout.smoothie_pricing_screen,null, false);
        mainLayout.addView(smoothiePricing);

        smoothiePricing.findViewById(R.id.increasePrice).setOnClickListener(increasePrice);
        smoothiePricing.findViewById(R.id.lowerPrice).setOnClickListener(lowerPrice);
        smoothiePricing.findViewById(R.id.cancelPricing).setOnClickListener(cancelPricing);
        smoothiePricing.findViewById(R.id.finalizePricing).setOnClickListener(finalizePricing);

        smoothiePricing.findViewById(R.id.clickedOffPricingScreen).setOnClickListener(
                clickedElsewhereWhilePricingListener);

        updatePriceTextView();

        smoothiePricing.setVisibility(View.VISIBLE);
    }

    private void updatePriceTextView(){
        TextView price = (TextView) smoothiePricing.findViewById(R.id.smoothiePricing);
        price.setText("$" + tempRecipe.getPrice().toString());
    }

    View.OnClickListener increasePrice = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            tempRecipe.setPrice(tempRecipe.getPrice() + 1);
            updatePriceTextView();
        }
    };

    View.OnClickListener lowerPrice = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (tempRecipe.getPrice() >= 1) {
                tempRecipe.setPrice(tempRecipe.getPrice() - 1);
                updatePriceTextView();
            }
        }
    };

    View.OnClickListener cancelPricing = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            tempRecipe.setPrice(0);
            updatePriceTextView();
            smoothiePricing.setVisibility(View.GONE);
        }
    };

    View.OnClickListener clickedElsewhereWhilePricingListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            cancelPricing.onClick(null);
        }
    };

    private void clearRecipe(){
        tempRecipe = new Recipe();
        LinearLayout recipeImages = (LinearLayout) creationScreen.findViewById(R.id.recipe);
        recipeImages.removeViews(0, recipeImages.getChildCount() - 1);
    }

    View.OnClickListener finalizePricing = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            List<Recipe> recipes = gameView.getRecipes();
            if (!recipes.contains(tempRecipe)){
                Recipe recipeToAdd = new Recipe(tempRecipe);
                recipeToAdd.generateName();
                recipeListAdapter.add(recipeToAdd);
                cancelPricing.onClick(null);
                clearRecipe();
                updateCost();
                // SHOULD NOTIFY THAT RECIPE HAS BEEN SAVED HERE SOMEHOW
            }
            else {
                // SOMEHOW NOTIFY THAT EXACT SAME RECIPE ALREADY EXISTS
            }
        }
    };
}

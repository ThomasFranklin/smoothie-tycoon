package com.tadgames.smoothietycoon.GameClasses;

import android.util.Pair;

/*  So yeah. Managers should cost quite a bit. Removes register limit and also provides some sort
    of bonus to the store in addition. Also retains normal employee stats so that it can work.
 */

public class Manager extends Employee{
    private Pair<String, Float> bonus;
    private long duration;

    public Manager(String name, String desc, int ticksRegister, int tickSmoothie, int attitude,
                   float nonPlayRevenue, int level, int exp, float cost, String bonusName,
                   float bonusAmount, long duration){
        super(name, desc,ticksRegister,tickSmoothie,attitude,nonPlayRevenue,level,exp,cost);

        bonus = new Pair<>(bonusName,bonusAmount);
        this.duration = duration;
    }

    public Pair<String, Float> getBonus(){
        return bonus;
    }

    public long getDuration(){
        return duration;
    }
}
